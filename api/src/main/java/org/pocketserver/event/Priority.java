package org.pocketserver.event;

public enum Priority {
    FIRST,
    DEFAULT,
    LAST,
    MONITOR,
}
