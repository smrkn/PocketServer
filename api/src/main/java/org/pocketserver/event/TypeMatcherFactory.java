package org.pocketserver.event;

public abstract class TypeMatcherFactory {
    private static TypeMatcherFactory instance;

    protected static void setInstance(TypeMatcherFactory instance) {
        TypeMatcherFactory.instance = instance;
    }

    static TypeMatcherFactory getInstance() {
        return instance;
    }

    protected abstract TypeMatcher getMatcher(Object obj, Class<?> clazz, String parameterName);

    protected static abstract class TypeMatcher {

        protected abstract boolean matches(Object object);

    }
}
