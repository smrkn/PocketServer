package org.pocketserver.event;

public interface EventHandler {

    void eventTriggered(Event event, Priority priority, EventSource source);
}
