package org.pocketserver.event;

public interface EventPipeline {

    void fireEvent(Event event, EventSource source);

    PipelineEditor edit();

    interface PipelineEditor {

        boolean add(EventHandler handler);

        boolean add(EventHandler handler, Priority... priorities);

        boolean addLast(EventHandler handler);

        boolean remove(EventHandler handler);

        void finish();

    }
}
