package org.pocketserver.event;

@SuppressWarnings("UnusedParameters")
public abstract class EventHandlerAdapter implements EventHandler {

    protected void eventTriggered(Event event) {

    }

    @Override
    public void eventTriggered(Event event, Priority priority, EventSource source) {
        if (!shouldFire(event)) {
            return;
        }
        if (priority == Priority.DEFAULT) {
            eventTriggered(event);
        }
    }

    protected boolean shouldFire(Event event) {
        return true; //This is default to true so it should be every single time
    }
}
