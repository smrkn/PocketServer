package org.pocketserver.event;

public abstract class SimpleEventHandler<E extends Event> extends EventHandlerAdapter {
    private final TypeMatcherFactory.TypeMatcher matcher;

    protected SimpleEventHandler(TypeMatcherFactory factory) {
        this.matcher = factory.getMatcher(this, SimpleEventHandler.class, "E");
    }

    protected SimpleEventHandler() {
        this.matcher = TypeMatcherFactory.getInstance().getMatcher(this, SimpleEventHandler.class, "E");
    }

    protected void eventCalled(E e) {

    }

    @Override
    protected final void eventTriggered(Event event) {
        //noinspection unchecked
        eventCalled((E) event);
    }

    @Override
    protected boolean shouldFire(Event event) {
        return this.matcher.matches(event);
    }
}
