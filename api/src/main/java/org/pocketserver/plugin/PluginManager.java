package org.pocketserver.plugin;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface PluginManager {
    List<Plugin> getPluginList();

    void loadPlugin(File file) throws IOException;

    void unloadPlugin(Plugin plugin);
}
