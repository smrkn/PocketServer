package org.pocketserver.plugin;

import org.pocketserver.Server;
import org.pocketserver.event.EventSource;

public abstract class Plugin implements EventSource {
    private Server server;
    private PluginManager manager;
    private boolean enabled;

    final void init(Server server, PluginManager pluginManager) {
        if (this.enabled) {
            throw new IllegalPluginException("This plugin has already been initialized.");
        }
        this.server = server;
        this.manager = pluginManager;
    }

    public Server getServer() {
        return server;
    }

    public PluginManager getManager() {
        return manager;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
