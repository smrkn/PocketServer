package org.pocketserver.plugin;

public class IllegalPluginException extends RuntimeException {

    public IllegalPluginException() {
        super();
    }

    public IllegalPluginException(String message) {
        super(message);
    }
}
