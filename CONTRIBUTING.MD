## Contributing

Before we are able to accept your contributions, we require you to [sign the Contributor License Agreement](https://www.clahub.com/agreements/PocketServer/PocketServer).

## Code Style

Pretty much a simple "no wildcards" and "please use this import order" request.
A minimal IntelliJ code style XML file has been placed in the repository root.

Contributions not using the style above may be rejected.

#### Importing The Profile

1. Create `~/.IntelliJIdea15/config/codestyles` if it does not already exist. The 15 may be different based on your IDE version.
2. Drop the `style.xml` file into that directory.
3. Select the "Pocket" profile from `Settings | Editor | Code Style`.

## Tests

If you can write unit (or integration) tests for components you contribute then great! We won't hunt you down if you don't/can't though. 

Before accepting a Pull Request we will first run any test suites and then give it a quick spin if applicable. If we encounter no errors then we'll happily merge it, but if not you'll have to deal with us grumpy gits. 

Currently only TestNG has been added to the POM, though if needed we're happy for PowerMockito to be added too! 

## Goals

We have no plans to work on an API until we have a mostly functional vanilla-esque
server implementation. Pull Requests contributing API specific features will
be unhesitantly rejected.
