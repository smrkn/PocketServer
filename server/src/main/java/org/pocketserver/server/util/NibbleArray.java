package org.pocketserver.server.util;

public class NibbleArray {
    private final byte[] bytes;

    public NibbleArray(int capacity) {
        this.bytes = new byte[capacity / 2];
    }

    public NibbleArray(byte[] bytes) {
        this.bytes = bytes;
    }

    public void set(int index, byte value) {
        index = index / 2;
        byte previous = bytes[index];
        if (index % 2 == 0) {
            bytes[index] = (byte) (previous & 0xF0 | value);
        } else {
            bytes[index] = (byte) (previous & 0x0F | value << 4);
        }
    }

    public byte get(int index) {
        byte value = bytes[index / 2];
        return (byte) (index % 2 == 0 ? value & 0x0f : value & 0xf0 >> 4);
    }

    public byte[] getRaw() {
        return bytes;
    }
}
