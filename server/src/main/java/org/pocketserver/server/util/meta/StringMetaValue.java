package org.pocketserver.server.util.meta;

import io.netty.buffer.ByteBuf;

import com.google.common.base.Charsets;

public class StringMetaValue extends MetaValue<String> {
    private static final int DATA_VALUE = 4;

    public StringMetaValue(String value) {
        super(DATA_VALUE, value);
    }

    @Override
    public void write(ByteBuf buf) {
        byte[] data = getValue().getBytes(Charsets.UTF_8);
        buf.writeShort(data.length);
        buf.writeBytes(data);
    }
}