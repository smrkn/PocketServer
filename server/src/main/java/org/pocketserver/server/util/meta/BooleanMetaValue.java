package org.pocketserver.server.util.meta;

import io.netty.buffer.ByteBuf;

public class BooleanMetaValue extends MetaValue<Boolean> {
    private static final int DATA_VALUE = 0;

    public BooleanMetaValue(boolean value) {
        super(DATA_VALUE, value);
    }

    @Override
    public void write(ByteBuf buf) {
        buf.writeBoolean(getValue());
    }
}