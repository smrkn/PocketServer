package org.pocketserver.server.util.meta;

import io.netty.buffer.ByteBuf;

public class ByteMetaValue extends MetaValue<Byte> {
    private static final int DATA_VALUE = 0;

    public ByteMetaValue(int value) {
        super(DATA_VALUE, (byte) value);
    }

    @Override
    public void write(ByteBuf buf) {
        buf.writeByte(getValue());
    }
}