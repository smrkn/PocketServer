package org.pocketserver.server.util;

public final class NibbleUtils {

    private NibbleUtils() {
        throw new UnsupportedOperationException("NibbleUtils cannot be initialized");
    }

    public static void addNibbleToArray(int[] bytes, NibbleArray nibbles) {
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = bytes[i] + nibbles.get(i);
        }
    }

    public static NibbleArray convertArrayToNibble(byte[] bytes) {
        int length = bytes.length / 2;
        if (length % 2 == 1) {
            length++;
        }
        NibbleArray array = new NibbleArray(length);
        for (int i = 0; i < bytes.length; i++) {
            array.set(i, bytes[i]);
        }
        return array;
    }
}
