package org.pocketserver.server.util;

import org.pocketserver.server.util.meta.MetaValue;

import java.util.HashMap;
import java.util.Map;

public class MetadataTable {
    private final Map<Integer, MetaValue> values = new HashMap<>();

    public void put(int id, MetaValue value) {
        this.values.put(id, value);
    }

    @SuppressWarnings("unchecked")
    public <E extends MetaValue> E getValue(int id) {
        return (E) values.get(id);
    }

    public Map<Integer, MetaValue> getValues() {
        return values;
    }
}
