package org.pocketserver.server.util.meta;

import io.netty.buffer.ByteBuf;

public abstract class MetaValue<E> {
    private final int type;
    private final E value;

    public MetaValue(int type, E value) {
        this.type = type;
        this.value = value;
    }

    public int getType() {
        return type;
    }

    public E getValue() {
        return value;
    }

    public abstract void write(ByteBuf buf);
}
