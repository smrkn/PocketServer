package org.pocketserver.server.util.meta;

import io.netty.buffer.ByteBuf;

public class ShortMetaValue extends MetaValue<Short> {
    private static final int DATA_VALUE = 1;

    public ShortMetaValue(int value) {
        super(DATA_VALUE, (short) value);
    }

    @Override
    public void write(ByteBuf buf) {
        buf.writeShort(getValue());
    }
}