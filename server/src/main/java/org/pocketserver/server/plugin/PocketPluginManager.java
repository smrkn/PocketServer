package org.pocketserver.server.plugin;

import org.pocketserver.plugin.Plugin;
import org.pocketserver.plugin.PluginManager;
import org.pocketserver.server.event.PocketEventPipeline;
import org.pocketserver.server.event.TypeMatcherSupplier;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.List;
import java.util.ServiceLoader;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.jar.JarFile;

public class PocketPluginManager implements PluginManager {
    private final PocketEventPipeline pipeline = new PocketEventPipeline();
    private final List<Plugin> pluginList = new CopyOnWriteArrayList<>();

    public PocketPluginManager() {
        new TypeMatcherSupplier(); //Do this to set the reference for pipeline
    }

    public PocketEventPipeline getEventPipeline() {
        return pipeline;
    }

    @Override
    public List<Plugin> getPluginList() {
        return pluginList;
    }

    @Override
    public void loadPlugin(File file) throws IOException {

    }

    @Override
    public void unloadPlugin(Plugin plugin) {

    }
}
