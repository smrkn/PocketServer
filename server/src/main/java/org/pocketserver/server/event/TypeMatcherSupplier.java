package org.pocketserver.server.event;

import org.pocketserver.event.TypeMatcherFactory;

import io.netty.util.internal.TypeParameterMatcher;

public class TypeMatcherSupplier extends TypeMatcherFactory {

    public TypeMatcherSupplier() {
        setInstance(this);
    }

    @Override
    protected TypeMatcher getMatcher(Object obj, Class<?> clazz, String parameterName) {
        return new NettyTypeMatcherWrapper(TypeParameterMatcher.find(obj, clazz, parameterName));
    }

    private class NettyTypeMatcherWrapper extends TypeMatcher {
        private final TypeParameterMatcher matcher;

        private NettyTypeMatcherWrapper(TypeParameterMatcher matcher) {
            this.matcher = matcher;
        }

        @Override
        protected boolean matches(Object object) {
            return this.matcher.match(object);
        }
    }
}
