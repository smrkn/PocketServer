package org.pocketserver.server.event;

import org.pocketserver.event.Event;
import org.pocketserver.event.EventHandler;
import org.pocketserver.event.EventPipeline;
import org.pocketserver.event.EventSource;
import org.pocketserver.event.Priority;

import com.google.common.base.Preconditions;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;

import java.util.Collection;

public class PocketEventPipeline implements EventPipeline {
    private volatile Multimap<Priority, EventHandler> handlers;

    public PocketEventPipeline() {
        handlers = new ImmutableListMultimap.Builder<Priority, EventHandler>().build();
    }

    @Override
    public void fireEvent(Event event, EventSource source) {
        for (Priority priority : Priority.values()) {
            Collection<EventHandler> handlers = this.handlers.get(priority);
            handlers.forEach(handler -> handler.eventTriggered(event, priority, source));
        }
    }

    @Override
    public EventPipeline.PipelineEditor edit() {
        return new PocketPipelineEditor();
    }

    private void updatePipeline(PocketPipelineEditor editor) {
        this.handlers = ImmutableMultimap.copyOf(editor.handlerEdits);
    }

    private class PocketPipelineEditor implements PipelineEditor {
        private final Multimap<Priority, EventHandler> handlerEdits;
        private boolean active;

        private PocketPipelineEditor() {
            this.handlerEdits = ArrayListMultimap.create(handlers);
            this.active = true;
        }

        @Override
        public boolean add(EventHandler handler) {
            return this.add(handler, Priority.DEFAULT);
        }

        @Override
        public boolean add(EventHandler handler, Priority... priorities) {
            Preconditions.checkState(active, "This pipeline editor is no longer valid.");
            synchronized (handlerEdits) {
                for (Priority priority : priorities) {
                    if (!this.handlerEdits.put(priority, handler)) {
                        return false;
                    }
                }
                return true;
            }
        }

        @Override
        public boolean addLast(EventHandler handler) {
            return this.add(handler, Priority.LAST);
        }

        @Override
        public boolean remove(EventHandler handler) {
            Preconditions.checkState(active, "This pipeline editor is no longer valid.");
            synchronized (handlerEdits) {
                for (Priority priority : Priority.values()) {
                    if (handlers.containsEntry(priority, handler)) {
                        this.handlerEdits.remove(priority, handler);
                        return true;
                    }
                }
                return false;
            }
        }

        @Override
        public synchronized void finish() {
            Preconditions.checkState(active, "This pipeline editor is no longer valid.");
            updatePipeline(this);
            this.active = false;
        }
    }
}
