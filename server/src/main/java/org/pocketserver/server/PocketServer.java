package org.pocketserver.server;

import org.pocketserver.Server;
import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketWrapper;
import org.pocketserver.server.net.PipelineUtil;
import org.pocketserver.server.net.packet.PacketPlayOutDisconnect;
import org.pocketserver.server.player.PocketPlayer;
import org.pocketserver.server.plugin.PocketPluginManager;
import org.pocketserver.server.world.PocketWorld;
import org.pocketserver.server.world.WorldSettings;
import org.pocketserver.server.world.provider.ChunkProvider;
import org.pocketserver.server.world.provider.anvil.AnvilChunkProvider;
import org.pocketserver.server.world.provider.flat.FlatChunkProvider;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.gson.Gson;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.InetSocketAddress;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class PocketServer implements Server {
    private static PocketServer instance;
    private final Map<InetSocketAddress, Session> sessions;
    private final Map<String, Supplier<ChunkProvider>> providers;
    private final ListeningExecutorService executor;
    private final EventLoopGroup eventLoopGroup;
    private final List<PocketWorld> worlds;
    private final Logger logger;
    private final Gson gson;
    private final PocketPluginManager pluginManager;
    private Channel channel;
    private Timer timer;

    public PocketServer() {
        this.logger = LoggerFactory.getLogger("PocketServer");
        this.timer = new Timer("PocketServer Packet Sender");
        this.eventLoopGroup = PipelineUtil.newEventLoop(6);
        this.sessions = Maps.newConcurrentMap();
        this.executor = MoreExecutors.listeningDecorator(Executors.newSingleThreadExecutor());
        this.worlds = new CopyOnWriteArrayList<>();
        this.gson = new Gson();
        this.providers = ImmutableMap.of("Flat", FlatChunkProvider::new, "Anvil", () -> new AnvilChunkProvider("world"));
        this.pluginManager = new PocketPluginManager();
        instance = this;

        loadWorlds();
        startListener();
    }

    public static PocketServer getInstance() {
        return instance;
    }

    private void startListener() {
        ChannelFutureListener listener = future -> {
            if (future.isSuccess()) {
                logger.info("Accepting connections on port 19132");
                channel = future.channel();
                timer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        for (Session session : sessions.values()) {
                            if (!session.hasPacketQueued()) {
                                continue;
                            }

                            Session.State state = session.getState();
                            if (!(state == Session.State.CONNECTING || state == Session.State.CONNECTED)) {
                                session.clearPacketQueue();
                                continue;
                            }

                            channel.eventLoop().submit(() -> {
                                PacketWrapper wrapper = null;
                                ChannelFuture future = null;
                                Iterator<PacketWrapper> iterator = session.iterator();
                                while ((wrapper = iterator.next()) != null) {
                                    future = channel.write(wrapper);
                                    iterator.remove();
                                }
                                if (future != null) {
                                    channel.flush();
                                }
                            });
                        }
                    }
                }, 10L, 10L);
            } else {
                logger.error("Failed to bind to port: ", future.cause());
                System.exit(1);
            }
        };

        new Bootstrap()
                .channel(PipelineUtil.getChannelClass())
                .handler(PipelineUtil.INITIALIZER)
                .group(eventLoopGroup)
                .option(ChannelOption.SO_BROADCAST, true)
                .bind(19132)
                .addListener(listener);
    }

    private void loadWorlds() {
        File home = new File("/");
        for (File directory : home.listFiles(File::isDirectory)) {
            File file = new File(directory, "settings.json");
            if (!file.exists()) {
                continue;
            }
            try {
                WorldSettings settings = this.gson.fromJson(new FileReader(file), WorldSettings.class);
                this.worlds.add(new PocketWorld(settings, providers.get(settings.getProvider()).get()));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        if (worlds.isEmpty()) {
            WorldSettings settings = new WorldSettings("Anvil", "world", 0, 100, 0, 0, (byte) 0, 1);
            this.worlds.add(new PocketWorld(settings, providers.get("Anvil").get()));
            getLogger().info("Created new world, 'world' using a " + settings.getProvider() + " generator.");
        }
    }

    public Logger getLogger() {
        return logger;
    }

    public Map<InetSocketAddress, Session> getSessions() {
        return sessions;
    }

    public ListeningExecutorService getExecutor() {
        return executor;
    }

    public List<PocketWorld> getWorlds() {
        return worlds;
    }

    public void shutdown() {
        broadcast(new PacketPlayOutDisconnect("The server is being shutdown."));
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        eventLoopGroup.shutdownGracefully();
        System.exit(0);
    }

    public Optional<PocketPlayer> getPlayer(String name) {
        return sessions.values().stream().filter(session -> {
            PocketPlayer player = session.getPlayer();
            return player != null && player.getName().equalsIgnoreCase(name);
        }).findFirst().map(Session::getPlayer);
    }

    public Optional<PocketPlayer> getPlayer(InetSocketAddress address) {
        return Optional.ofNullable(sessions.get(address)).map(Session::getPlayer);
    }

    public void broadcast(Packet packet) {
        broadcast(packet, player -> true);
    }

    public void broadcast(Packet packet, Collection<PocketPlayer> recipients) {
        if (recipients.isEmpty()) {
            return;
        }
        recipients.forEach(player -> player.getSession().send(packet));
    }

    public void broadcast(Packet packet, Predicate<PocketPlayer> predicate) {
        sessions.values().stream().forEach(session -> {
            PocketPlayer player = session.getPlayer();
            if (player != null && predicate.test(player)) {
                session.send(packet);
            }
        });
    }
}
