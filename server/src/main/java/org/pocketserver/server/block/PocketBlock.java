package org.pocketserver.server.block;

import com.google.common.base.Preconditions;

import java.util.Optional;

public class PocketBlock {
    private final int x;
    private final int y;
    private final int z;
    private volatile Material material;

    public PocketBlock(Material material, int x, int y, int z) {
        this.material = material;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Material getMaterial() {
        return this.material;
    }

    public void setMaterial(Material material) {
        Preconditions.checkNotNull(material);
        this.material = material;
    }

    public int getId() {
        return this.material.getId();
    }

    public void setId(int id) {
        Optional<Material> material = Material.getMaterial(id);
        this.setMaterial(material.orElseThrow(NullPointerException::new));
    }
}
