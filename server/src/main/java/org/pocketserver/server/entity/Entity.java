package org.pocketserver.server.entity;

import org.pocketserver.server.util.MetadataTable;
import org.pocketserver.server.util.meta.BooleanMetaValue;
import org.pocketserver.server.util.meta.ByteMetaValue;
import org.pocketserver.server.util.meta.ShortMetaValue;
import org.pocketserver.server.util.meta.StringMetaValue;
import org.pocketserver.server.world.Location;
import org.pocketserver.server.world.PocketWorld;

import java.util.concurrent.atomic.AtomicInteger;

public class Entity {
    private static final AtomicInteger ID = new AtomicInteger(0);
    private final MetadataTable metadata;
    private final int id;
    private Location location;
    private boolean aiEnabled;

    public Entity(Location location) {
        this.metadata = new MetadataTable() {{
            put(0, new ByteMetaValue(0));
            put(1, new ShortMetaValue(300));
            put(2, new StringMetaValue(""));
            put(3, new BooleanMetaValue(true));
            put(4, new BooleanMetaValue(false));
            put(15, new BooleanMetaValue(aiEnabled));
        }};
        this.location = location;
        this.id = ID.getAndIncrement();
    }

    public int getEntityId() {
        return id;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public PocketWorld getWorld() {
        return location.getWorld();
    }

    public MetadataTable getMetadata() {
        return metadata;
    }

    public boolean isAiEnabled() {
        return aiEnabled;
    }

    public void setAiEnabled(boolean aiEnabled) {
        this.aiEnabled = aiEnabled;
    }
}
