package org.pocketserver.server.item.inventory;

import org.pocketserver.server.block.Material;
import org.pocketserver.server.item.ItemStack;
import org.pocketserver.server.player.PocketPlayer;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterators;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

public class PlayerInventory implements Inventory, Serializable {
    private final ItemStack[] contents;
    private final String title;
    private AtomicInteger emptyIndex = new AtomicInteger(0);
    private int size;

    public PlayerInventory(ItemStack[] contents, PocketPlayer player) {
        this.contents = contents;
        this.title = player.getName().concat("'s inventory");
        recalculateIndex();
    }

    public PlayerInventory(int capacity, PocketPlayer player) {
        this(new ItemStack[capacity], player);
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void addItem(ItemStack stack) {
        Preconditions.checkNotNull(stack);
        if (stack.getMaterial() == Material.AIR) {
            return;
        }
        setItem(emptyIndex.getAndIncrement(), stack);
        size++;
        recalculateIndex();
    }

    @Override
    public void setItem(int index, ItemStack item) {
        Preconditions.checkArgument(index >= 0, "Index must be greater or equal to 0");
        Preconditions.checkArgument(index <= contents.length, "Index must be less than or equal to inventory size");
        Preconditions.checkNotNull(item);
        contents[index] = item;
        if (emptyIndex.intValue() > index && item.getMaterial() == Material.AIR) {
            emptyIndex.set(index);
        }
    }

    @Override
    public Collection<ItemStack> getContents() {
        if (contents == null || contents.length == 0 || Arrays.equals(contents, new ItemStack[contents.length])) {
            return ImmutableList.of();
        }
        return ImmutableList.copyOf(contents);
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public int getCapacity() {
        return contents.length;
    }

    @Override
    public boolean contains(ItemStack stack) {
        for (ItemStack content : contents) {
            if (content.equals(stack)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int getEmptyIndex() {
        return emptyIndex.get();
    }

    @Override
    public int indexOf(ItemStack stack) {
        return 0;
    }

    @Override
    public Collection<InventoryViewer> getViewers() {
        return null;
    }

    @Override
    public Iterator<ItemStack> iterator() {
        return Iterators.cycle(contents);
    }

    @Override
    public void forEach(Consumer<? super ItemStack> action) {
        getContents().forEach(action);
    }

    @Override
    public Spliterator<ItemStack> spliterator() {
        return getContents().spliterator();
    }

    private void recalculateIndex() {
        int index = emptyIndex.get();
        if (index >= getCapacity()) {
            emptyIndex.set(-1);
            return;
        }
        ItemStack content = contents[index];
        if (content == null || content.getMaterial() == Material.AIR) {
            return;
        }
        for (int i = index; i < contents.length; i++) {
            content = contents[i];
            if (content == null || content.getMaterial() == Material.AIR) {
                emptyIndex.set(i);
                return;
            }
        }
        emptyIndex.set(-1);
    }
}
