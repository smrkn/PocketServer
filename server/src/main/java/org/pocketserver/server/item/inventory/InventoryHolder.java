package org.pocketserver.server.item.inventory;

public interface InventoryHolder {

    Inventory getInventory();

}
