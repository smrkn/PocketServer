package org.pocketserver.server.item.inventory.types;

import org.pocketserver.server.item.inventory.PlayerInventory;
import org.pocketserver.server.player.PocketPlayer;

public class SurvivalInventory extends PlayerInventory {
    private static final int SURVIVAL_INVENTORY_SIZE = 36;

    public SurvivalInventory(PocketPlayer player) {
        super(SURVIVAL_INVENTORY_SIZE, player);
    }

}
