package org.pocketserver.server.item.meta;

import java.util.LinkedList;
import java.util.List;

public class ItemMeta {
    private final List<MetaValue> meta = new LinkedList<>();

    public void addMeta(MetaValue value) {
        this.meta.add(value);
    }

    public void removeMeta(MetaValue value) {
        this.meta.remove(value);
    }
}
