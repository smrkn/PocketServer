package org.pocketserver.server.item.meta;

public class MetaValue<E> {
    private final E value;

    public MetaValue(E value) {
        this.value = value;
    }

    public E getValue() {
        return value;
    }
}
