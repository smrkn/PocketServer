package org.pocketserver.server.item.inventory;

public interface InventoryViewer {

    /**
     * Checks if the inventory view is viewing an inventory
     *
     * @return if the viewer currently has an inventory open
     */
    boolean isViewing();

    /**
     * Gives the inventory that a viewer currently has opened.
     *
     * @return the inventory the viewer is looking at or null if not viewing
     */
    Inventory getViewing();
}
