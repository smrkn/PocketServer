package org.pocketserver.server.item;

import org.pocketserver.server.block.Material;

import java.io.Serializable;

public class ItemStack implements Serializable {
    private final Material material;
    private final int amount;
    private final int data;

    public ItemStack(Material material, int amount, int data) {
        this.material = material;
        this.amount = amount;
        this.data = data;
    }

    public Material getMaterial() {
        return material;
    }

    public int getAmount() {
        return amount;
    }

    public int getData() {
        return data;
    }
}
