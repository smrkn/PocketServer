package org.pocketserver.server.item.inventory;

import org.pocketserver.server.item.ItemStack;

import java.util.Collection;

public interface Inventory extends Iterable<ItemStack> {

    String getTitle();

    void addItem(ItemStack stack);

    void setItem(int index, ItemStack item);

    Collection<ItemStack> getContents();

    int getSize();

    int getCapacity();

    boolean contains(ItemStack stack);

    int getEmptyIndex();

    int indexOf(ItemStack stack);

    Collection<InventoryViewer> getViewers();
}
