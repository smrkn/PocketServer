package org.pocketserver.server.item.inventory.types;

import org.pocketserver.server.item.ItemStack;
import org.pocketserver.server.item.inventory.PlayerInventory;
import org.pocketserver.server.player.PocketPlayer;

public class CreativeInventory extends PlayerInventory {
    private static final int CREATIVE_INVENTORY_SIZE = 112;

    public CreativeInventory(PocketPlayer player) {
        super(CREATIVE_INVENTORY_SIZE, player);
    }

    @Override
    public void setItem(int index, ItemStack item) {
        throw new UnsupportedOperationException("Inventory#setItem is not supported in a creative inventory!");
    }
}
