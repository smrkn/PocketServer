package org.pocketserver.server;

import org.slf4j.impl.SimpleLogger;

public final class PocketBootstrap {
    public static void main(String[] args) {
        System.setProperty(SimpleLogger.ROOT_LOGGER_NAME, "PocketServer");
        System.setProperty(SimpleLogger.DEFAULT_LOG_LEVEL_KEY, "debug");
        System.setProperty(SimpleLogger.LEVEL_IN_BRACKETS_KEY, "true");
        System.setProperty(SimpleLogger.SHOW_THREAD_NAME_KEY, "false");
        System.setProperty(SimpleLogger.SHOW_DATE_TIME_KEY, "false");

        PocketServer server = new PocketServer();
        while (true) {

        }
    }
}
