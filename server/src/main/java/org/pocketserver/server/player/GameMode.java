package org.pocketserver.server.player;

public enum GameMode {
    SURVIVAL,
    CREATIVE,
    SPECTATOR;

    public int getId() {
        return ordinal();
    }
}
