package org.pocketserver.server.player;

import org.pocketserver.server.PocketServer;
import org.pocketserver.server.Session;
import org.pocketserver.server.entity.Entity;
import org.pocketserver.server.item.inventory.Inventory;
import org.pocketserver.server.item.inventory.InventoryHolder;
import org.pocketserver.server.item.inventory.InventoryViewer;
import org.pocketserver.server.item.inventory.PlayerInventory;
import org.pocketserver.server.item.inventory.types.CreativeInventory;
import org.pocketserver.server.item.inventory.types.SurvivalInventory;
import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.packet.PacketPlayOutAdventureSettings;
import org.pocketserver.server.net.packet.PacketPlayOutText;
import org.pocketserver.server.world.Location;

import com.google.common.base.MoreObjects;

import java.util.Optional;
import java.util.UUID;

public class PocketPlayer extends Entity implements InventoryHolder, InventoryViewer {
    private final String name;
    private final Unsafe unsafe;
    private final UUID uuid;
    private final PlayerInventory inventory;
    private int viewDistance;
    private GameMode gameMode;
    private boolean allowFly;
    private boolean flying;
    private boolean autoJump;

    public PocketPlayer(String name, UUID uuid, int viewDistance, Session playerSession, Location location) {
        super(location);
        this.name = name;
        this.uuid = uuid;
        this.viewDistance = viewDistance;
        this.inventory = gameMode == GameMode.SURVIVAL ? new SurvivalInventory(this) : new CreativeInventory(this);
        this.unsafe = new Unsafe() {
            private final Session session = playerSession;

            @Override
            public void sendPacket(Packet packet) {
                this.session.send(packet, true);
            }

            @Override
            public Session getSession() {
                return session;
            }
        };
    }

    public String getName() {
        return name;
    }

    public Unsafe getUnsafe() {
        return unsafe;
    }

    public int getViewDistance() {
        return viewDistance;
    }

    public void setViewDistance(int viewDistance) {
        this.viewDistance = viewDistance;
    }

    public void chat(String message) {
        //TODO: Format the message
        PacketPlayOutText text = new PacketPlayOutText(PacketPlayOutText.TextType.CHAT, message, Optional.of(name));
        PocketServer instance = PocketServer.getInstance();
        instance.getLogger().info("[Chat] {}: {}", name, message);
        instance.broadcast(text);
    }

    public Session getSession() {
        return unsafe.getSession();
    }

    public UUID getUniqueId() {
        return uuid;
    }

    public GameMode getGameMode() {
        return gameMode;
    }

    public void setGameMode(GameMode gameMode) {
        this.gameMode = gameMode;
    }

    public boolean isFlying() {
        return flying;
    }

    public void setFlying(boolean flying) {
        this.flying = flying;
    }

    public void sendSettings() {
        Packet packet = new PacketPlayOutAdventureSettings(this);
        getUnsafe().sendPacket(packet);
    }

    @Override
    public Inventory getInventory() {
        return this.inventory;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("id", getEntityId())
                .toString();
    }

    @Override
    public boolean isViewing() {
        return getViewing() != null;
    }

    @Override
    public Inventory getViewing() {
        return null; //TODO: Implement this
    }

    public boolean hasAutoJump() {
        return autoJump;
    }

    public void setAutoJump(boolean autoJump) {
        this.autoJump = autoJump;
    }

    public boolean hasFlyEnabled() {
        return allowFly;
    }

    public void setFlyEnabled(boolean allowFly) {
        this.allowFly = allowFly;
    }

    public interface Unsafe {
        void sendPacket(Packet packet);

        Session getSession();
    }
}
