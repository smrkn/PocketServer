package org.pocketserver.server.world.provider;

import org.pocketserver.server.world.PocketChunk;

import java.util.concurrent.Future;

public interface ChunkProvider {

    Future<PocketChunk> loadChunk(int x, int z);

}
