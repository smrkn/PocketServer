package org.pocketserver.server.world;

import org.pocketserver.server.block.Material;
import org.pocketserver.server.block.PocketBlock;
import org.pocketserver.server.net.packet.PacketPlayOutBatch;
import org.pocketserver.server.util.NibbleArray;

import com.google.common.base.Preconditions;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import java.lang.ref.SoftReference;
import java.util.Arrays;
import java.util.Optional;
import java.util.function.Function;

public class PocketChunk {
    private final PocketBlock[] blocks;
    private final NibbleArray data;
    private final NibbleArray lighting;
    private final NibbleArray skylight;
    private final NibbleArray height;
    private final byte[] biomes;
    private final byte[] biomeColors;
    private final int x;
    private final int z;
    private byte[] cache;
    private boolean dirty;
    private SoftReference<PacketPlayOutBatch> batch;

    public PocketChunk(int x, int z) {
        this.x = x;
        this.z = z;
        this.blocks = new PocketBlock[32768];
        this.data = new NibbleArray(32768);
        this.lighting = new NibbleArray(32768);
        this.skylight = new NibbleArray(32768);
        this.height = new NibbleArray(16 * 16);
        this.biomes = new byte[16 * 16];
        this.biomeColors = new byte[16 * 16 * 3];
        for (x = 0; x < 16; x++) {
            for (int y = 0; y < 128; y++) {
                for (z = 0; z < 16; z++) {
                    blocks[getBlockIndex(x, y, z)] = new PocketBlock(Material.AIR, x + this.x, y, +z + this.z);
                }
            }
        }
        for (int i = 0; i < biomeColors.length; i += 3) {
            biomeColors[i] = 127;
            biomeColors[i + 1] = 102;
            biomeColors[i + 2] = 25;
        }
    }

    public int getX() {
        return x;
    }

    public int getZ() {
        return z;
    }

    public void setBlock(int x, int y, int z, Material material) {
        getBlock(x, y, z).setMaterial(material);
        dirty = true;
    }

    public PocketBlock getBlock(int x, int y, int z) {
        Preconditions.checkArgument(x <= 16);
        Preconditions.checkArgument(y <= 128);
        Preconditions.checkArgument(z <= 16);
        return blocks[getBlockIndex(x, y, z)];
    }

    private int getBlockIndex(int x, int y, int z) {
        return (x * 2048) + (z * 128) + y;
    }

    public byte[] getData() {
        if (this.cache == null || this.dirty) {
            ByteArrayDataOutput output = ByteStreams.newDataOutput();
            output.write(toByteArray(blocks, pocketBlock -> (byte) pocketBlock.getId()));
            output.write(data.getRaw());
            output.write(lighting.getRaw());
            output.write(skylight.getRaw());
            output.write(height.getRaw());

            for (int i = 0; i < this.biomes.length; ++i) {
                byte r = this.biomeColors[i * 3];
                byte g = this.biomeColors[i * 3 + 1];
                byte b = this.biomeColors[i * 3 + 2];
                int color = r << 16 | g << 8 | b;
                output.writeInt(24 << this.biomes[i] | color & 0xffffff);
            }
            output.writeInt(0);

            this.dirty = false;
            this.cache = output.toByteArray();
        }
        return this.cache;
    }

    private <E> byte[] toByteArray(E[] objects, Function<E, Byte> mapper) {
        byte[] bytes = new byte[objects.length];
        for (int i = 0; i < objects.length; i++) {
            bytes[i] = mapper.apply(objects[i]);
            Arrays.stream(objects);
        }
        return bytes;
    }

    public Optional<PacketPlayOutBatch> getBatch() {
        if (this.batch == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(this.batch.get());
    }

    public void setBatch(PacketPlayOutBatch batch) {
        this.batch = new SoftReference<>(batch);
    }

    public boolean isDirty() {
        return dirty;
    }
}
