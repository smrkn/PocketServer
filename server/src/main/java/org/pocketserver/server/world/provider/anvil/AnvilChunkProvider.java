package org.pocketserver.server.world.provider.anvil;

import org.pocketserver.nbt.tags.NbtByteArrayTag;
import org.pocketserver.nbt.tags.NbtByteTag;
import org.pocketserver.nbt.tags.NbtCompoundTag;
import org.pocketserver.nbt.tags.NbtListTag;
import org.pocketserver.server.block.Material;
import org.pocketserver.server.util.NibbleArray;
import org.pocketserver.server.util.NibbleUtils;
import org.pocketserver.server.world.PocketChunk;
import org.pocketserver.server.world.provider.ChunkProvider;
import org.pocketserver.server.world.provider.flat.FlatChunkProvider;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class AnvilChunkProvider implements ChunkProvider {
    private final ListeningExecutorService executor;
    private final File directory;
    private final Cache<String, RegionFile> regionCache;

    public AnvilChunkProvider(String path) {
        this.executor = MoreExecutors.listeningDecorator(Executors.newSingleThreadExecutor());
        File directory = new File(path);
        if (!directory.exists()) {
            boolean success = directory.mkdir();
            if (!success) {
                throw new RuntimeException("The anvil directory doesn't exist and cannot be created.");
            }
        }
        this.directory = directory;
        this.regionCache = CacheBuilder.newBuilder().expireAfterAccess(1, TimeUnit.MINUTES).build();
    }

    public AnvilChunkProvider(File directory) {
        this.executor = MoreExecutors.listeningDecorator(Executors.newSingleThreadExecutor());
        this.directory = directory;
        this.regionCache = CacheBuilder.newBuilder().expireAfterAccess(1, TimeUnit.MINUTES).build();
    }

    @Override
    public Future<PocketChunk> loadChunk(int x, int z) {
        return executor.submit(() -> {
            int fileX = x >> 5;
            int fileZ = z >> 5;
            String name = String.format("r.%s.%s.mca", fileX, fileZ);
            RegionFile region = this.regionCache.get(name, () -> new RegionFile(new File(directory, name)));
            return readChunk(region, x, z);
        });
    }

    private PocketChunk readChunk(RegionFile regionFile, int x, int z) {
        NbtCompoundTag root = regionFile.readLocation(x, z);
        if (root == null) {
            try {
                return new FlatChunkProvider().loadChunk(x, z).get(3, TimeUnit.SECONDS);
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                e.printStackTrace();
            }
            return null;
        }
        PocketChunk chunk = new PocketChunk(x, z);
        NbtCompoundTag level = root.getTag("Level");
        NbtListTag sections = level.getTag("Sections");
        List<NbtCompoundTag> tags = sections.getTags();
        for (NbtCompoundTag tag : tags) {
            NbtByteTag yIndex = tag.getTag("Y");
            byte value = yIndex.getValue();
            if (value != 0 && value != 1) { //Any higher and the map doesn't generate anymore (Check heightmap/lighting)
                continue;
            }
            byte[] byteBlocks = ((NbtByteArrayTag) tag.getTag("Blocks")).getValue();
            int[] blocks = convertToIntArray(byteBlocks);
            tag.getOptionalTag("Add").ifPresent(add -> {
                NibbleArray added = NibbleUtils.convertArrayToNibble(((NbtByteArrayTag) add).getValue());
                NibbleUtils.addNibbleToArray(blocks, added);
            });
            NibbleArray data = new NibbleArray(((NbtByteArrayTag) tag.getTag("Data")).getValue());
            for (int chunkX = 0; chunkX < 16; chunkX++) {
                for (int chunkY = 0; chunkY < 16; chunkY++) {
                    for (int chunkZ = 0; chunkZ < 16; chunkZ++) {
                        int index = chunkY * 16 * 16 + chunkZ * 16 + chunkX;
                        int block = blocks[index];
                        //int block = Math.random() > .5 ? 1 : 2;
                        //if (block > 2) {
                        //    continue;
                        //}
                        Optional<Material> material = Material.getMaterial(block);
                        chunk.setBlock(chunkX, chunkY + (value * 16), chunkZ, material.get());
                    }
                }
            }
        }
        return chunk;
    }

    private int[] convertToIntArray(byte[] byteArray) {
        int[] ints = new int[byteArray.length];
        for (int i = 0; i < ints.length; i++) {
            ints[i] = Byte.toUnsignedInt(byteArray[i]);
        }
        return ints;
    }
}
