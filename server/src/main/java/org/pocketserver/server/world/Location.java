package org.pocketserver.server.world;

public class Location {
    private final PocketWorld world;
    private final double x;
    private final double y;
    private final double z;
    private final float pitch;
    private final float yaw;

    public Location(PocketWorld world, int x, int y, int z) {
        this(world, x, y, z, 0, 0);
    }

    public Location(PocketWorld world, int x, int y, int z, float pitch, float yaw) {
        this.world = world;
        this.x = x;
        this.y = y;
        this.z = z;
        this.pitch = pitch;
        this.yaw = yaw;
    }

    public PocketWorld getWorld() {
        return world;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public float getPitch() {
        return pitch;
    }

    public float getYaw() {
        return yaw;
    }

    public double distanceSquared(Location location) {
        return Math.pow(location.getX() - x, 2) + Math.pow(location.getY() - y, 2) + Math.pow(location.getZ() - z, 2);
    }

    public double distance(Location location) {
        return Math.sqrt(distanceSquared(location));
    }
}
