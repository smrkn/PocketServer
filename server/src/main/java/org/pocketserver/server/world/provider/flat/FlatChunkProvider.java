package org.pocketserver.server.world.provider.flat;

import org.pocketserver.server.block.Material;
import org.pocketserver.server.world.PocketChunk;
import org.pocketserver.server.world.provider.ChunkProvider;

import com.google.common.util.concurrent.Futures;

import java.util.concurrent.Future;

public class FlatChunkProvider implements ChunkProvider {
    @Override
    public Future<PocketChunk> loadChunk(int x, int z) {
        PocketChunk chunk = new PocketChunk(x, z);
        for (x = 0; x < 16; x++) {
            for (int y = 0; y < 16; y++) {
                chunk.setBlock(x, 0, y, Material.BEDROCK);
                for (int i = 1; i < 3; i++) {
                    chunk.setBlock(x, i, y, Material.STONE);
                }
                chunk.setBlock(x, 3, y, Material.GRASS);
            }
        }
        return Futures.immediateFuture(chunk);
    }
}
