package org.pocketserver.server.world;

import org.pocketserver.server.PocketServer;
import org.pocketserver.server.Session;
import org.pocketserver.server.block.Material;
import org.pocketserver.server.block.PocketBlock;
import org.pocketserver.server.net.packet.PacketPlayOutAddPlayer;
import org.pocketserver.server.net.packet.PacketPlayOutBatch;
import org.pocketserver.server.net.packet.PacketPlayOutChunkData;
import org.pocketserver.server.net.packet.PacketPlayOutPlayerStatus;
import org.pocketserver.server.net.packet.PacketPlayOutStartGame;
import org.pocketserver.server.player.PocketPlayer;
import org.pocketserver.server.world.provider.ChunkProvider;

import io.netty.buffer.ByteBufAllocator;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class PocketWorld {
    private final Table<Integer, Integer, PocketChunk> chunks;
    private final WorldSettings settings;
    private final Set<PocketPlayer> players;
    private final ChunkProvider provider;

    public PocketWorld(WorldSettings settings, ChunkProvider provider) {
        this.settings = settings;
        this.provider = provider;
        this.chunks = HashBasedTable.create();
        this.players = new CopyOnWriteArraySet<>();
    }

    public synchronized PocketChunk getChunk(int x, int z) {
        if (this.chunks.contains(x, z)) {
            return this.chunks.get(x, z);
        }
        try {
            PocketChunk chunk = provider.loadChunk(x, z).get(5, TimeUnit.SECONDS);
            this.chunks.put(x, z, chunk);
            return chunk;
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }

    public PocketChunk getChunk(int x, int y, int z) { //TODO: Replace with location
        return getChunk(x >> 4, z >> 4);
    }

    public void addPlayer(PocketPlayer player) {
        if (!players.add(player)) {
            PocketServer.getInstance().getLogger().debug("{} attempted to join the same world multiple times.", player);
            return;
        }
        PocketPlayer.Unsafe unsafe = player.getUnsafe();
        unsafe.sendPacket(new PacketPlayOutStartGame(settings, player));
        player.sendSettings();
        PocketServer.getInstance().broadcast(new PacketPlayOutAddPlayer(player, getSpawn()), p -> p != player);
    }

    public void sendInitialChunks(PocketPlayer player) {
        PocketPlayer.Unsafe unsafe = player.getUnsafe();
        //int distance = player.getViewDistance();
        int distance = 8;
        PocketServer.getInstance().getExecutor().submit(() -> {
            Session session = unsafe.getSession();
            for (int x = 0 - distance; x < distance; x++) {
                for (int z = 0 - distance; z < distance; z++) {
                    PocketChunk chunk = getChunk(x, z);
                    Optional<PacketPlayOutBatch> batch = chunk.getBatch();
                    if (false && batch.isPresent() && !chunk.isDirty()) { //Something isn't right when it caches it so
                        session.send(batch.get(), true);                  // false is there while testing
                        continue;
                    }
                    PacketPlayOutChunkData chunkData = new PacketPlayOutChunkData(x, z, chunk.getData());
                    session.compressPacket(ByteBufAllocator.DEFAULT, chunkData, (packet) -> {
                        session.send(packet, true);
                        chunk.setBatch(packet);
                    });
                }
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            unsafe.sendPacket(new PacketPlayOutPlayerStatus(PacketPlayOutPlayerStatus.Status.SPAWNED));
        });
    }

    public WorldSettings getSettings() {
        return settings;
    }

    public Collection<PocketPlayer> getViewers(PocketPlayer player) {
        List<PocketPlayer> viewers = new ArrayList<>();
        for (PocketPlayer other : players) {
            if (other.equals(player)) {
                continue;
            }
            /*
            Location location = other.getLocation();
            double distance = Math.pow(other.getViewDistance(), 2);
            if (location.distanceSquared(player.getLocation()) <= distance) {
                viewers.add(other);
            }
            */
            viewers.add(other);
        }
        return viewers;
    }

    public Location getSpawn() {
        return new Location(this, settings.getSpawnX(), settings.getSpawnY(), settings.getSpawnZ());
    }

    public void setBlock(int x, int y, int z, Material material) {
        int chunkX = x >> 4;
        int chunkZ = z >> 4;
        getChunk(chunkX, z >> 4).setBlock(x - (chunkX * 16), y, z - (chunkZ * 16), material);
    }

    public PocketBlock getBlock(int x, int y, int z) {
        int chunkX = x >> 4;
        int chunkZ = z >> 4;
        return getChunk(chunkX, z >> 4).getBlock(x - (chunkX * 16), y, z - (chunkZ * 16));
    }
}
