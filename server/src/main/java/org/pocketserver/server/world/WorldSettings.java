package org.pocketserver.server.world;

public class WorldSettings {
    private final String provider;
    private String name;
    private int spawnX;
    private int spawnY;
    private int spawnZ;
    private int seed;
    private byte dimension;
    private int gamemode;
    private boolean editEnabled;

    public WorldSettings(String provider, String name, int spawnX, int spawnY, int spawnZ, int seed, byte dimension, int gamemode) {
        this.provider = provider;
        this.name = name;
        this.spawnX = spawnX;
        this.spawnY = spawnY;
        this.spawnZ = spawnZ;
        this.seed = seed;
        this.dimension = dimension;
        this.gamemode = gamemode;
    }

    public String getProvider() {
        return provider;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSpawnX() {
        return spawnX;
    }

    public void setSpawnX(int spawnX) {
        this.spawnX = spawnX;
    }

    public int getSpawnY() {
        return spawnY;
    }

    public void setSpawnY(int spawnY) {
        this.spawnY = spawnY;
    }

    public int getSpawnZ() {
        return spawnZ;
    }

    public void setSpawnZ(int spawnZ) {
        this.spawnZ = spawnZ;
    }

    public int getSeed() {
        return seed;
    }

    public void setSeed(int seed) {
        this.seed = seed;
    }

    public byte getDimension() {
        return dimension;
    }

    public int getGamemode() {
        return gamemode;
    }

    public boolean isEditEnabled() {
        return editEnabled;
    }

    public void setEditEnabled(boolean editEnabled) {
        this.editEnabled = editEnabled;
    }
}
