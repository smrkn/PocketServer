package org.pocketserver.server.world.provider.anvil;

import org.pocketserver.nbt.NbtReader;
import org.pocketserver.nbt.tags.NbtCompoundTag;

import com.google.common.base.Objects;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.InflaterInputStream;

class RegionFile {
    private transient final RandomAccessFile file;
    private final Map<ChunkCoordinate, NbtCompoundTag> chunkCompounds;

    RegionFile(File file) {
        if (!file.exists()) {
            this.file = null;
            this.chunkCompounds = null;
            return;
        }
        RandomAccessFile temp;
        try {
            temp = new RandomAccessFile(file, "rw");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            temp = null;
        }
        this.file = temp;
        this.chunkCompounds = new HashMap<>();
    }

    NbtCompoundTag readLocation(int x, int z) {
        if (file == null || chunkCompounds == null) {
            return null;
        }
        return chunkCompounds.computeIfAbsent(new ChunkCoordinate(x, z), coordinate -> {
            try {
                synchronized (file) {
                    int loc = 4 * ((x & 31) + (z & 31) * 32);
                    file.seek(loc);

                    byte[] offset = new byte[3];
                    file.read(offset);
                    int medium = readMedium(offset);
                    int sectors = file.read();
                    if (medium == 0 && sectors == 0) {
                        return null;
                    }

                    file.seek(medium * 4096);
                    int size = file.readInt();
                    byte type = file.readByte();

                    byte[] bytes = new byte[size];
                    file.read(bytes);
                    InputStream stream = new ByteArrayInputStream(bytes);
                    if (type == 2) {
                        stream = new InflaterInputStream(stream);
                    }
                    return new NbtReader(stream).readCompound();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        });
    }

    private int readMedium(byte[] payload) {
        return (payload[0] & 0xff) << 16 | (payload[1] & 0xff) << 8 | payload[2] & 0xff;
    }

    private class ChunkCoordinate {
        private final int x;
        private final int z;

        private ChunkCoordinate(int x, int z) {
            this.x = x;
            this.z = z;
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(this.x, this.z);
        }
    }
}
