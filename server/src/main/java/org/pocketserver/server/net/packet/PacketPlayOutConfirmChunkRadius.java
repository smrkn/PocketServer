package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketPlayOutConfirmChunkRadius extends Packet {
    private final int radius;

    public PacketPlayOutConfirmChunkRadius(int radius) {
        super(PacketType.CONFIRM_CHUNK_RADIUS);
        this.radius = radius;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeInt(radius);
    }
}
