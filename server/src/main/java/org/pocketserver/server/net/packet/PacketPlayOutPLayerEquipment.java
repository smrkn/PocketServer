package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketPlayOutPLayerEquipment extends Packet {

    public PacketPlayOutPLayerEquipment() {
        super(PacketType.PLAYER_EQUIPMENT);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {

    }
}
