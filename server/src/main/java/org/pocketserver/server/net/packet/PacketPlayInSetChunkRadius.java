package org.pocketserver.server.net.packet;

import org.pocketserver.server.PocketServer;
import org.pocketserver.server.Session;
import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;
import org.pocketserver.server.net.PipelineUtil;
import org.pocketserver.server.player.PocketPlayer;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

import java.util.List;

public class PacketPlayInSetChunkRadius extends Packet {
    private int radius;

    public PacketPlayInSetChunkRadius() {
        super(PacketType.SET_CHUNK_RADIUS);
    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        Session session = PocketServer.getInstance().getSessions().get(ctx.attr(PipelineUtil.ADDRESS_ATTRIBUTE).get());
        session.getPlayer().setViewDistance(radius);
        session.send(new PacketPlayOutConfirmChunkRadius(radius), true, session1 -> {
            PocketPlayer player = session1.getPlayer();
            player.getWorld().sendInitialChunks(player);
        });
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        this.radius = buf.readInt();
    }
}
