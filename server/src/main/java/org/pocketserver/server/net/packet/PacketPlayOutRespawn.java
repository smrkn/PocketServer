package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketPlayOutRespawn extends Packet {
    private float x;
    private float y;
    private float z;

    public PacketPlayOutRespawn() {
        super(PacketType.RESPAWN);
    }

    public PacketPlayOutRespawn(float x, float y, float z) {
        this();
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeFloat(x);
        buf.writeFloat(y);
        buf.writeFloat(z);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        x = buf.readFloat();
        y = buf.readFloat();
        z = buf.readFloat();
    }
}
