package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketPlayOutRemoveEntity extends Packet {
    private final long entityId;

    public PacketPlayOutRemoveEntity(long entityId) {
        super(PacketType.REMOVE_ENTITY);
        this.entityId = entityId;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeLong(entityId);
    }
}
