package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;
import org.pocketserver.server.player.GameMode;
import org.pocketserver.server.player.PocketPlayer;
import org.pocketserver.server.world.PocketWorld;

import io.netty.buffer.ByteBuf;

public class PacketPlayOutAdventureSettings extends Packet {
    private boolean editEnabled;
    private boolean autoJump;
    private boolean flying;
    private boolean spectator;
    private boolean noclip;
    private int userPermission;
    private int globalPermission;

    public PacketPlayOutAdventureSettings(PocketPlayer player) {
        this();
        PocketWorld world = player.getWorld();
        this.editEnabled = world.getSettings().isEditEnabled();
        this.autoJump = player.hasAutoJump();
        this.flying = player.hasFlyEnabled();
        this.spectator = player.getGameMode() == GameMode.SPECTATOR;
    }

    public PacketPlayOutAdventureSettings() {
        super(PacketType.ADVENTURE_SETTINGS);
        this.userPermission = 2;
        this.globalPermission = 2;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeInt(getFlag());
        buf.writeInt(userPermission);
        buf.writeInt(globalPermission);
    }

    private int getFlag() {
        int flag = 0;
        if (editEnabled) { //Prints a ton of messages on each click
            flag |= 0x01;
        }
        if (autoJump) { //Only if enabled in client settings
            flag |= 0x40;
        }
        if (flying) {
            flag |= 0x80;
        }
        if (spectator) { //Players are able to go through blocks
            flag |= 0x100;
        }
        return flag;
    }

    public void setAutoJump(boolean autoJump) {
        this.autoJump = autoJump;
    }

    public void setFlying(boolean flying) {
        this.flying = flying;
    }

    public void setSpectator(boolean spectator) {
        this.spectator = spectator;
    }

    public void setUserPermission(int userPermission) {
        this.userPermission = userPermission;
    }

    public void setGlobalPermission(int globalPermission) {
        this.globalPermission = globalPermission;
    }

    public void setEditEnabled(boolean editEnabled) {
        this.editEnabled = editEnabled;
    }
}
