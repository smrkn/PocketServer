package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketPlayOutContainerSetSlot extends Packet {

    public PacketPlayOutContainerSetSlot() {
        super(PacketType.CONTAINER_SET_SLOT);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {

    }
}
