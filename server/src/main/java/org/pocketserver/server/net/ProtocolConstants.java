package org.pocketserver.server.net;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.security.SecureRandom;
import java.util.regex.Pattern;

public final class ProtocolConstants {
    public static final Pattern DISALLOWED_CHARACTERS = Pattern.compile("[^" + Pattern.quote(" \u00A7!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~") + "]");
    public static final long SERVER_ID = new SecureRandom().nextLong();
    public static final int PROTOCOL_VERSION = 60;
    public static final int RAKNET_VERSION = 7;
    public static final ByteBuf RAKNET_OFFLINE_MESSAGE_ID;

    static {
        ByteBuf magic = Unpooled.buffer();
        magic.writeByte(0x00);
        magic.writeByte(0xFF);
        magic.writeByte(0xFF);
        magic.writeByte(0x00);
        magic.writeByte(0xFE);
        magic.writeByte(0xFE);
        magic.writeByte(0xFE);
        magic.writeByte(0xFE);
        magic.writeByte(0xFD);
        magic.writeByte(0xFD);
        magic.writeByte(0xFD);
        magic.writeByte(0xFD);
        magic.writeByte(0x12);
        magic.writeByte(0x34);
        magic.writeByte(0x56);
        magic.writeByte(0x78);
        RAKNET_OFFLINE_MESSAGE_ID = Unpooled.unmodifiableBuffer(magic);
    }

    private ProtocolConstants() {
        throw new UnsupportedOperationException("ProtocolConstants cannot be instantiated!");
    }
}
