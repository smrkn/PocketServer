package org.pocketserver.server.net.packet;

import org.pocketserver.server.block.Material;
import org.pocketserver.server.item.ItemStack;
import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;
import org.pocketserver.server.player.PocketPlayer;
import org.pocketserver.server.util.MetadataTable;
import org.pocketserver.server.world.Location;

import io.netty.buffer.ByteBuf;

import java.util.UUID;

public class PacketPlayOutAddPlayer extends Packet {
    private UUID uuid;
    private String name;
    private long entityId;
    private float x;
    private float y;
    private float z;
    private float speedX;
    private float speedY;
    private float speedZ;
    private float yaw;
    private float headYaw;
    private float pitch;
    private MetadataTable table;

    public PacketPlayOutAddPlayer(UUID uuid, String name, long entityId, float x, float y, float z,
                                  float speedX, float speedY, float speedZ, float yaw, float headYaw, float pitch,
                                  MetadataTable table) {
        super(PacketType.ADD_PLAYER);
        this.uuid = uuid;
        this.name = name;
        this.entityId = entityId;
        this.x = x;
        this.y = y;
        this.z = z;
        this.speedX = speedX;
        this.speedY = speedY;
        this.speedZ = speedZ;
        this.yaw = yaw;
        this.headYaw = headYaw;
        this.pitch = pitch;
        this.table = table;
    }

    public PacketPlayOutAddPlayer() {
        super(PacketType.ADD_PLAYER);
    }

    public PacketPlayOutAddPlayer(PocketPlayer player, Location location) {
        this(player.getUniqueId(), player.getName(), player.getEntityId(), (float) location.getX(),
                (float) location.getY(), (float) location.getZ(), 0, 0, 0, location.getYaw(),
                location.getYaw(), location.getPitch(), player.getMetadata());
    }


    @Override
    public void write(ByteBuf buf) throws Exception {
        writeUUID(buf, uuid);
        writeString(buf, name);
        buf.writeLong(entityId);
        buf.writeFloat(x);
        buf.writeFloat(y);
        buf.writeFloat(z);
        buf.writeFloat(speedX);
        buf.writeFloat(speedY);
        buf.writeFloat(speedZ);
        buf.writeFloat(yaw);
        buf.writeFloat(headYaw);
        buf.writeFloat(pitch);
        writeItem(buf, new ItemStack(Material.AIR, 1, 1));
        writeMetadata(buf, table);
    }
}
