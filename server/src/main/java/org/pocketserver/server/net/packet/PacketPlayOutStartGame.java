package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;
import org.pocketserver.server.player.PocketPlayer;
import org.pocketserver.server.world.WorldSettings;

import io.netty.buffer.ByteBuf;

public class PacketPlayOutStartGame extends Packet {
    private final int seed;
    private final byte dimension; /* TODO: Replace this with a proper enum. */
    private final int generator; /* TODO: Replace this with a proper enum. */
    private final int gamemode; /* TODO: Replace this with a proper enum. */
    private final long entityId;
    private final int spawnX;
    private final int spawnY;
    private final int spawnZ;
    private final float x;
    private final float y;
    private final float z;
    private final String unknwon;

    public PacketPlayOutStartGame(WorldSettings settings, PocketPlayer player) {
        this(settings.getSeed(), settings.getDimension(), 0, settings.getGamemode(), player.getEntityId(),
                settings.getSpawnX(), settings.getSpawnY(), settings.getSpawnZ(),
                settings.getSpawnX(), settings.getSpawnY(), settings.getSpawnZ());
    }

    public PacketPlayOutStartGame(int seed, byte dimension, int generator, int gamemode, long entityId, int spawnX, int spawnY, int spawnZ, float x, float y, float z) {
        super(PacketType.START_GAME);
        this.seed = seed;
        this.dimension = dimension;
        this.generator = generator;
        this.gamemode = gamemode;
        this.entityId = entityId;
        this.spawnX = spawnX;
        this.spawnY = spawnY;
        this.spawnZ = spawnZ;
        this.x = x;
        this.y = y;
        this.z = z;
        this.unknwon = ""; //TODO: Figure out what this unknown is
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeInt(seed);
        buf.writeByte(dimension);
        buf.writeInt(generator);
        buf.writeInt(gamemode);
        buf.writeLong(entityId);
        buf.writeInt(spawnX);
        buf.writeInt(spawnY);
        buf.writeInt(spawnZ);
        buf.writeFloat(x);
        buf.writeFloat(y);
        buf.writeFloat(z);
        buf.writeByte(1);
        buf.writeByte(1);
        buf.writeByte(0);
        writeString(buf, unknwon);
    }
}
