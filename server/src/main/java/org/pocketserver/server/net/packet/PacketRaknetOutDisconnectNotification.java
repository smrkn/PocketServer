package org.pocketserver.server.net.packet;

import org.pocketserver.server.PocketServer;
import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;
import org.pocketserver.server.net.PipelineUtil;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

import java.net.InetSocketAddress;
import java.util.List;

public class PacketRaknetOutDisconnectNotification extends Packet {
    public PacketRaknetOutDisconnectNotification() {
        super(PacketType.RAKNET_DISCONNECT_NOTIFICATION);
    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        InetSocketAddress address = PipelineUtil.getAddress(ctx);
        if (PocketServer.getInstance().getSessions().remove(address) != null) {
            PocketServer.getInstance().getLogger().info("{} has disconnected", address);
        }
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        // NOP
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        // NOP
    }
}
