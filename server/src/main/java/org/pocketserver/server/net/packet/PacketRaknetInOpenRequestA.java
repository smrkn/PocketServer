package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;
import org.pocketserver.server.net.ProtocolConstants;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

import java.util.List;

public class PacketRaknetInOpenRequestA extends Packet {
    private byte raknetVersion;
    private int mtu;

    public PacketRaknetInOpenRequestA() {
        super(PacketType.RAKNET_OPEN_REQUEST_A);
    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        if (raknetVersion == ProtocolConstants.RAKNET_VERSION) {
            out.add(new PacketRaknetOutOpenResponseA(mtu));
        } else {
            out.add(new PacketRaknetOutIncompatibleProtocol());
        }
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        buf.skipBytes(16);
        this.raknetVersion = buf.readByte();
        this.mtu = buf.readableBytes();
    }
}
