package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;
import org.pocketserver.server.net.ProtocolConstants;

import io.netty.buffer.ByteBuf;

import java.net.InetSocketAddress;

public class PacketRaknetOutOpenResponseB extends Packet {
    private final InetSocketAddress address;
    private final short mtu;

    public PacketRaknetOutOpenResponseB(InetSocketAddress address, short mtu) {
        super(PacketType.RAKNET_OPEN_RESPONSE_B);
        this.address = address;
        this.mtu = mtu;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        Packet.writeOfflineMessageId(buf);
        buf.writeLong(ProtocolConstants.SERVER_ID);
        writeAddress(buf, address);
        buf.writeShort(mtu);
        buf.writeByte(0);
    }
}
