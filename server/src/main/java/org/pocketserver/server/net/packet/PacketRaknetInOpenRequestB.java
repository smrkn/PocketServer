package org.pocketserver.server.net.packet;

import org.pocketserver.server.PocketServer;
import org.pocketserver.server.Session;
import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;
import org.pocketserver.server.net.PipelineUtil;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

import java.net.InetSocketAddress;
import java.util.List;

public class PacketRaknetInOpenRequestB extends Packet {
    private boolean secure;
    private int cookie;
    private short port;
    private short mtu;
    private long clientId;

    public PacketRaknetInOpenRequestB() {
        super(PacketType.RAKNET_OPEN_REQUEST_B);
    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        InetSocketAddress remote = PipelineUtil.getAddress(ctx);
        Session session = PocketServer.getInstance().getSessions().compute(remote, (address, old) -> {
            if (old != null) {
                if (old.getState() == Session.State.CONNECTING) {
                    return old;
                }
                PocketServer.getInstance().getLogger().info("Invalid session state detected, initial connection attempt may have failed?");
            }
            old = new Session(ctx.channel(), address);
            old.setMtu(mtu);
            PocketServer.getInstance().getLogger().debug("Setting the MTU for {} to {}.", old, mtu);
            return old;
        });

        if (session.getState() == Session.State.NOT_CONNECTED) {
            session.setState(Session.State.CONNECTING);
            out.add(new PacketRaknetOutOpenResponseB(remote, port));
        }
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        buf.skipBytes(16);
        this.secure = buf.readBoolean();
        this.cookie = buf.readInt();
        this.port = buf.readShort();
        this.mtu = buf.readShort();
        this.clientId = buf.readLong();
    }
}
