package org.pocketserver.server.net;

import org.pocketserver.server.PocketServer;
import org.pocketserver.server.block.Material;
import org.pocketserver.server.item.ItemStack;
import org.pocketserver.server.util.MetadataTable;
import org.pocketserver.server.util.meta.MetaValue;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.net.InetAddresses;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public abstract class Packet {
    private final PacketType type;

    protected Packet(PacketType type) {
        this.type = type;
    }

    protected static void writeAddress(ByteBuf buf, InetSocketAddress address) {
        Preconditions.checkArgument(!address.isUnresolved(), "address is not resolved");
        InetAddress addr = address.getAddress();

        if (addr.getClass() == Inet6Address.class) {
            Inet6Address temp = (Inet6Address) addr;
            try {
                addr = InetAddresses.getEmbeddedIPv4ClientAddress(temp);
            } catch (IllegalArgumentException ex) {
                throw new IllegalArgumentException("address type not supported", ex);
            }
        }

        buf.writeByte(0x04);
        for (byte b : addr.getAddress()) {
            buf.writeByte(~b & 0xFF);
        }
        buf.writeShort(address.getPort());
    }

    protected static InetSocketAddress readAddress(ByteBuf buf) {
        Preconditions.checkArgument(buf.readByte() == 0x04, "address type not supported");
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 4; i++) {
            builder.append(~(buf.readByte() | ~0xFF));
            builder.append(".");
        }
        builder.setLength(builder.length() - 1);
        short port = buf.readShort();
        return new InetSocketAddress(builder.toString(), port);
    }

    protected static String readString(ByteBuf buf) {
        Preconditions.checkArgument(buf.isReadable(), "unable to read from buf");
        byte[] data = new byte[buf.readShort()];
        buf.readBytes(data);
        return new String(data, Charsets.UTF_8);
    }

    protected static void writeString(ByteBuf buf, String str) {
        Preconditions.checkArgument(buf.isWritable(), "unable to write to buf");
        str = ProtocolConstants.DISALLOWED_CHARACTERS.matcher(str).replaceAll("");
        byte[] data = str.getBytes(Charsets.UTF_8);
        buf.writeShort(data.length);
        buf.writeBytes(data);
    }

    protected static UUID readUUID(ByteBuf buf) {
        return new UUID(buf.readLong(), buf.readLong());
    }

    protected static void writeUUID(ByteBuf buf, UUID uuid) {
        buf.writeLong(uuid.getMostSignificantBits());
        buf.writeLong(uuid.getLeastSignificantBits());
    }

    protected static void writeOfflineMessageId(ByteBuf buf) {
        buf.writeBytes(ProtocolConstants.RAKNET_OFFLINE_MESSAGE_ID, 0, 16);
    }

    protected static int readInt24(ByteBuf buf) {
        Preconditions.checkArgument(buf.isReadable(3), "unable to read from buf");
        byte[] payload = new byte[3];
        buf.readBytes(payload);

        return (payload[0] & 0xFF | (payload[1] & 0xFF) << 8 | (payload[2] & 0xFF) << 16);
    }

    protected static void writeInt24(ByteBuf buf, int value) {
        Preconditions.checkArgument(buf.isWritable(3), "unable to write to buf");
        value = value & 0x00FFFFFF;
        buf.writeByte(value);
        buf.writeByte(value >> 8);
        buf.writeByte(value >> 16);
    }

    protected static void writeItem(ByteBuf buf, ItemStack itemStack) {
        if (itemStack == null || itemStack.getMaterial() == Material.AIR) {
            buf.writeShort(0);
            return;
        }
        buf.writeShort(itemStack.getMaterial().getId());
        buf.writeByte(itemStack.getAmount() & 0xff);
        buf.writeShort(-1);
        buf.writeShort(0);
        buf.writeBytes(new byte[0]);
    }

    protected static void writeMetadata(ByteBuf buf, MetadataTable metadata) {
        for (Map.Entry<Integer, MetaValue> entry : metadata.getValues().entrySet()) {
            MetaValue value = entry.getValue();
            buf.writeByte((value.getType() << 5 | (entry.getKey() & 0x1F)) & 0xff);
            value.write(buf);
        }
        buf.writeByte(0x7f);
    }

    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        PocketServer.getInstance().getLogger().debug(toString() + " doesn't implement Packet#handle");
    }

    public void write(ByteBuf buf) throws Exception {
        throw new UnsupportedOperationException("packet should implement write");
    }

    public void read(ByteBuf buf) throws Exception {
        throw new UnsupportedOperationException("packet should implement read");
    }

    public PacketType getType() {
        return type;
    }

    @Override
    public final String toString() {
        return String.format("Packet[id=0x%02X, type=%s]", type.getId(), type);
    }
}
