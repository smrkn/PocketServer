package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketRaknetOutDetectLostConnection extends Packet {
    public PacketRaknetOutDetectLostConnection() {
        super(PacketType.RAKNET_DETECT_LOST_CONNECTIONS);
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        // NOP
    }
}
