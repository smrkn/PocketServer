package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketPlayOutPlayerInteract extends Packet {

    public PacketPlayOutPlayerInteract() {
        super(PacketType.INTERACT);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {

    }
}
