package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketPlayOutSetSpawnPosition extends Packet {
    private final int x;
    private final int y;
    private final int z;

    public PacketPlayOutSetSpawnPosition(int x, int y, int z) {
        super(PacketType.SET_SPAWN_POSITION);
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeInt(x);
        buf.writeInt(y);
        buf.writeInt(z);
    }
}
