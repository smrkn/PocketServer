package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketPlayOutChunkData extends Packet {

    private final int x;
    private final int z;
    private final byte[] bytes;

    public PacketPlayOutChunkData(int x, int z, byte[] data) {
        super(PacketType.CHUNK_DATA);
        this.x = x;
        this.z = z;
        this.bytes = data;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeInt(x);
        buf.writeInt(z);
        buf.writeByte(0);
        buf.writeInt(bytes.length);
        buf.writeBytes(bytes);
    }
}
