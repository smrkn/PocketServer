package org.pocketserver.server.net.packet;

import org.pocketserver.server.PocketServer;
import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;
import org.pocketserver.server.net.PipelineUtil;
import org.pocketserver.server.player.PocketPlayer;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.util.Attribute;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.Optional;

public class PacketPlayOutRemoveBlock extends Packet {
    private long entityId;
    private int x;
    private byte y;
    private int z;

    public PacketPlayOutRemoveBlock() {
        super(PacketType.REMOVE_BLOCK);
    }

    public PacketPlayOutRemoveBlock(long entityId, int x, byte y, int z) {
        super(PacketType.REMOVE_BLOCK);
        this.entityId = entityId;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        Attribute<InetSocketAddress> attr = ctx.attr(PipelineUtil.ADDRESS_ATTRIBUTE);
        PocketServer instance = PocketServer.getInstance();
        InetSocketAddress address = attr.get();
        Optional<PocketPlayer> optional = instance.getPlayer(address);
        if (!optional.isPresent()) {
            instance.getLogger().debug("No player found for {}.", address);
            return;
        }
        PocketPlayer player = optional.get();
        if (player.getEntityId() != entityId) {
            instance.getLogger().debug("{} possibly spoofing block packets.", player);
            return;
        }
        //player.getWorld().setBlock(x, y, z, Material.AIR); //TODO: Debug why this doesn't set the correct positions
        PacketPlayOutUpdateBlock packet = new PacketPlayOutUpdateBlock(new PacketPlayOutUpdateBlock.BlockRecord(x, z, y, (byte) 0, (byte) 0));
        instance.broadcast(packet);
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeLong(entityId);
        buf.writeInt(x);
        buf.writeInt(z);
        buf.writeByte(y);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        entityId = buf.readLong();
        x = buf.readInt();
        z = buf.readInt();
        y = buf.readByte();
    }
}
