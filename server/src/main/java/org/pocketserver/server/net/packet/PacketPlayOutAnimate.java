package org.pocketserver.server.net.packet;

import org.pocketserver.server.PocketServer;
import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;
import org.pocketserver.server.net.PipelineUtil;
import org.pocketserver.server.player.PocketPlayer;
import org.pocketserver.server.world.PocketWorld;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.util.Attribute;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.Optional;

public class PacketPlayOutAnimate extends Packet {
    private byte action;
    private long entityId;

    public PacketPlayOutAnimate(byte action, long entityId) {
        this();
        this.action = action;
        this.entityId = entityId;
    }

    public PacketPlayOutAnimate() {
        super(PacketType.ANIMATE);
    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        Attribute<InetSocketAddress> attr = ctx.attr(PipelineUtil.ADDRESS_ATTRIBUTE);
        PocketServer instance = PocketServer.getInstance();
        Optional<PocketPlayer> optional = instance.getPlayer(attr.get());
        if (optional.isPresent()) {
            PocketPlayer player = optional.get();
            PocketWorld world = player.getWorld();
            instance.broadcast(this, world.getViewers(player));
        }
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeByte(action);
        buf.writeLong(entityId);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        this.action = buf.readByte();
        this.entityId = buf.readLong();
    }
}
