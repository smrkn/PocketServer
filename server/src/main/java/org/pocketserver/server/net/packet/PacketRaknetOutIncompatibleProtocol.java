package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;
import org.pocketserver.server.net.ProtocolConstants;

import io.netty.buffer.ByteBuf;

public class PacketRaknetOutIncompatibleProtocol extends Packet {
    public PacketRaknetOutIncompatibleProtocol() {
        super(PacketType.RAKNET_INCOMPATIBLE_PROTOCOL);
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeByte(ProtocolConstants.RAKNET_VERSION);
        Packet.writeOfflineMessageId(buf);
        buf.writeLong(ProtocolConstants.SERVER_ID);
    }
}
