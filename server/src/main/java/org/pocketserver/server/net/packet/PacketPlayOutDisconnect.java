package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketPlayOutDisconnect extends Packet {
    private final String reason;

    public PacketPlayOutDisconnect(String reason) {
        super(PacketType.DISCONNECT);
        this.reason = reason;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        writeString(buf, reason);
    }
}
