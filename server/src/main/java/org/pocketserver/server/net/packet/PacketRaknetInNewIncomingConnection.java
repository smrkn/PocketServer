package org.pocketserver.server.net.packet;

import org.pocketserver.server.PocketServer;
import org.pocketserver.server.Session;
import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;
import org.pocketserver.server.net.PipelineUtil;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

import java.net.InetSocketAddress;
import java.util.List;

public class PacketRaknetInNewIncomingConnection extends Packet {
    private int cookie;
    private boolean secure;
    private short port;
    private long sessionId1;
    private long sessionId2;

    public PacketRaknetInNewIncomingConnection() {
        super(PacketType.RAKNET_NEW_INCOMING_CONNECTION);
    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        InetSocketAddress address = PipelineUtil.getAddress(ctx);
        Session session = PocketServer.getInstance().getSessions().get(address);
        if (session.getState() != Session.State.CONNECTED) {
            PocketServer.getInstance().getLogger().info("Accepting connection from {}", address);
            session.setState(Session.State.CONNECTED);
        }
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        this.cookie = buf.readInt();
        this.secure = buf.readBoolean();
        this.port = buf.readShort();
        this.sessionId1 = buf.readLong();
        this.sessionId2 = buf.readLong();
    }
}
