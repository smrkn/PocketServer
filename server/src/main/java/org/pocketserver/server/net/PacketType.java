package org.pocketserver.server.net;

import org.pocketserver.server.net.packet.PacketConnectInPlayerLogin;
import org.pocketserver.server.net.packet.PacketPlayInPlayerAction;
import org.pocketserver.server.net.packet.PacketPlayInSetChunkRadius;
import org.pocketserver.server.net.packet.PacketPlayOutAddPainting;
import org.pocketserver.server.net.packet.PacketPlayOutAdventureSettings;
import org.pocketserver.server.net.packet.PacketPlayOutAnimate;
import org.pocketserver.server.net.packet.PacketPlayOutBatch;
import org.pocketserver.server.net.packet.PacketPlayOutContainerClose;
import org.pocketserver.server.net.packet.PacketPlayOutContainerOpen;
import org.pocketserver.server.net.packet.PacketPlayOutContainerSetData;
import org.pocketserver.server.net.packet.PacketPlayOutContainerSetSlot;
import org.pocketserver.server.net.packet.PacketPlayOutDropItem;
import org.pocketserver.server.net.packet.PacketPlayOutEntityEvent;
import org.pocketserver.server.net.packet.PacketPlayOutLevelEvent;
import org.pocketserver.server.net.packet.PacketPlayOutMovePlayer;
import org.pocketserver.server.net.packet.PacketPlayOutPLayerEquipment;
import org.pocketserver.server.net.packet.PacketPlayOutPlayerArmorEquipment;
import org.pocketserver.server.net.packet.PacketPlayOutPlayerInteract;
import org.pocketserver.server.net.packet.PacketPlayOutRemoveBlock;
import org.pocketserver.server.net.packet.PacketPlayOutRespawn;
import org.pocketserver.server.net.packet.PacketPlayOutText;
import org.pocketserver.server.net.packet.PacketPlayOutUpdateBlock;
import org.pocketserver.server.net.packet.PacketPlayOutUseItem;
import org.pocketserver.server.net.packet.PacketRaknetInConnectedPing;
import org.pocketserver.server.net.packet.PacketRaknetInNewIncomingConnection;
import org.pocketserver.server.net.packet.PacketRaknetInOpenConnectionRequest;
import org.pocketserver.server.net.packet.PacketRaknetInOpenRequestA;
import org.pocketserver.server.net.packet.PacketRaknetInOpenRequestB;
import org.pocketserver.server.net.packet.PacketRaknetInUnconnectedPing;
import org.pocketserver.server.net.packet.PacketRaknetOutAck;
import org.pocketserver.server.net.packet.PacketRaknetOutConnectionBanned;
import org.pocketserver.server.net.packet.PacketRaknetOutCustomPacket;
import org.pocketserver.server.net.packet.PacketRaknetOutDetectLostConnection;
import org.pocketserver.server.net.packet.PacketRaknetOutDisconnectNotification;
import org.pocketserver.server.net.packet.PacketRaknetOutNak;
import org.pocketserver.server.net.packet.PacketRaknetOutNoFreeIncomingConnections;

import java.util.function.Supplier;

import gnu.trove.impl.unmodifiable.TUnmodifiableByteObjectMap;
import gnu.trove.map.TByteObjectMap;
import gnu.trove.map.hash.TByteObjectHashMap;

public enum PacketType {
    RAKNET_CONNECTED_PING(0x00, true, PacketRaknetInConnectedPing::new),
    RAKNET_UNCONNECTED_PING(0x01, true, PacketRaknetInUnconnectedPing::new),
    RAKNET_CONNECTED_PONG(0x03, true),
    RAKNET_DETECT_LOST_CONNECTIONS(0x04, true, PacketRaknetOutDetectLostConnection::new),
    RAKNET_UNCONNECTED_PONG(0x1C, true),
    RAKNET_OPEN_REQUEST_A(0x05, true, PacketRaknetInOpenRequestA::new),
    RAKNET_OPEN_RESPONSE_A(0x06, true),
    RAKNET_OPEN_REQUEST_B(0x07, true, PacketRaknetInOpenRequestB::new),
    RAKNET_OPEN_RESPONSE_B(0x08, true),
    RAKNET_OPEN_CONNECTION_REQUEST(0x09, false, PacketRaknetInOpenConnectionRequest::new),
    RAKNET_OPEN_CONNECTION_REQUEST_ACCEPTED(0x10, false),
    RAKNET_NEW_INCOMING_CONNECTION(0x13, false, PacketRaknetInNewIncomingConnection::new),
    RAKNET_NO_FREE_INCOMING_CONNECTION(0x14, false, PacketRaknetOutNoFreeIncomingConnections::new),
    RAKNET_DISCONNECT_NOTIFICATION(0x15, false, PacketRaknetOutDisconnectNotification::new),
    RAKNET_CONNECTION_BANNED(0x17, false, PacketRaknetOutConnectionBanned::new),
    RAKNET_INCOMPATIBLE_PROTOCOL(0x1A, false),
    RAKNET_CUSTOM_PACKET(0x80, true, PacketRaknetOutCustomPacket::new),
    RAKNET_ACK(0xC0, true, PacketRaknetOutAck::new),
    RAKNET_NAK(0xA0, true, PacketRaknetOutNak::new),
    PLAYER_LOGIN(0x8F, false, PacketConnectInPlayerLogin::new),
    PLAYER_STATUS(0x90, false),
    DISCONNECT(0x91, false),
    BATCH(0x92, false, PacketPlayOutBatch::new),
    TEXT(0x93, false, PacketPlayOutText::new),
    START_GAME(0x95, false),
    ADD_PLAYER(0x96, false),
    REMOVE_ENTITY(0x99, false),
    MOVE_PLAYER(0x9D, false, PacketPlayOutMovePlayer::new),
    REMOVE_BLOCK(0x9E, false, PacketPlayOutRemoveBlock::new),
    UPDATE_BLOCK(0x9F, false, PacketPlayOutUpdateBlock::new),
    ADD_PAINTING(0xA0, false, PacketPlayOutAddPainting::new),
    LEVEL_EVENT(0xA2, false, PacketPlayOutLevelEvent::new),
    ENTITY_EVENT(0xA4, false, PacketPlayOutEntityEvent::new),
    PLAYER_EQUIPMENT(0xA7, false, PacketPlayOutPLayerEquipment::new),
    PLAYER_ARMOR_EQUIPMENT(0xA8, false, PacketPlayOutPlayerArmorEquipment::new),
    INTERACT(0xA9, false, PacketPlayOutPlayerInteract::new),
    USE_ITEM(0xAA, false, PacketPlayOutUseItem::new),
    PLAYER_ACTION(0xAB, false, PacketPlayInPlayerAction::new),
    SET_HEALTH(0xB0, false),
    SET_SPAWN_POSITION(0xB1, false),
    ANIMATE(0xB2, false, PacketPlayOutAnimate::new),
    RESPAWN(0xB3, false, PacketPlayOutRespawn::new),
    DROP_ITEM(0xB4, false, PacketPlayOutDropItem::new),
    CONTAINER_OPEN(0xB5, false, PacketPlayOutContainerOpen::new),
    CONTAINER_CLOSE(0xB6, false, PacketPlayOutContainerClose::new),
    CONTAINER_SET_SLOT(0xB7, false, PacketPlayOutContainerSetSlot::new),
    CONTAINER_SET_DATA(0xB8, false, PacketPlayOutContainerSetData::new),
    ADVENTURE_SETTINGS(0xBC, false, PacketPlayOutAdventureSettings::new),
    SET_CHUNK_RADIUS(0xC8, false, PacketPlayInSetChunkRadius::new),
    CONFIRM_CHUNK_RADIUS(0xC9, false),
    CHUNK_DATA(0xbf, false);

    private static final TByteObjectMap<PacketType> raknetMessages;
    private static final TByteObjectMap<PacketType> gameMessages;

    static {
        TByteObjectMap<PacketType> localRaknetMessages = new TByteObjectHashMap<>();
        TByteObjectMap<PacketType> localGameMessages = new TByteObjectHashMap<>();

        for (PacketType type : values()) {
            TByteObjectMap<PacketType> map = type.raw ? localRaknetMessages : localGameMessages;
            map.put(type.id, type);
        }

        for (int i = 0x80; i < 0x8F; i++) {
            localRaknetMessages.put((byte) i, PacketType.RAKNET_CUSTOM_PACKET);
        }

        raknetMessages = new TUnmodifiableByteObjectMap<>(localRaknetMessages);
        gameMessages = new TUnmodifiableByteObjectMap<>(localGameMessages);
    }

    private final Supplier<Packet> initializer;
    private final boolean raw;
    private final byte id;

    PacketType(Number id, boolean raw) {
        this(id, raw, () -> null);
    }

    PacketType(Number id, boolean raw, Supplier<Packet> initializer) {
        this.initializer = initializer;
        this.id = id.byteValue();
        this.raw = raw;
    }

    public static Packet create(byte id, boolean raw) {
        TByteObjectMap<PacketType> dictionary = raw ? raknetMessages : gameMessages;
        PacketType type = dictionary.get(id);
        if (type == null) {
            throw new MissingPacketException(id);
        }
        return type.create();
    }

    public Packet create() {
        return initializer.get();
    }

    public byte getId() {
        return id;
    }

    public boolean isRaw() {
        return raw;
    }
}
