package org.pocketserver.server.net.netty;

import org.pocketserver.server.PocketServer;
import org.pocketserver.server.Session;
import org.pocketserver.server.net.PacketWrapper;
import org.pocketserver.server.net.PipelineUtil;
import org.pocketserver.server.net.packet.PacketRaknetOutCustomPacket;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.socket.DatagramPacket;
import io.netty.handler.codec.MessageToMessageEncoder;

import java.util.List;
import java.util.function.Consumer;

public class PocketWrapperEncoder extends MessageToMessageEncoder<PacketWrapper> {
    @Override
    protected void encode(ChannelHandlerContext ctx, PacketWrapper msg, List<Object> out) throws Exception {
        ctx.attr(PipelineUtil.ADDRESS_ATTRIBUTE).set(msg.getRecipient());
        if (msg.getPacket().getType().isRaw()) {
            ctx.write(msg.getPacket());
            return;
        }
        PacketRaknetOutCustomPacket.writeMany(ctx, msg).forEach(outgoing -> {
            ByteBuf buffer = outgoing.getBuffer();
            if (PocketEncoder.dump) {
                PocketServer.getInstance().getLogger().debug("Encoded: {}", ByteBufUtil.hexDump(buffer).toUpperCase());
            }
            Consumer<Session> receipt = msg.getAckReceipt();
            if (receipt != null) {
                Session session = PocketServer.getInstance().getSessions().get(msg.getRecipient());
                session.addAckReceipt(outgoing.getSequenceNumber(), receipt);
            }
            out.add(new DatagramPacket(buffer, msg.getRecipient()));
        });
    }
}
