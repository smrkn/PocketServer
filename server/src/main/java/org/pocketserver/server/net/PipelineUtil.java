package org.pocketserver.server.net;

import org.pocketserver.server.net.netty.PocketDecoder;
import org.pocketserver.server.net.netty.PocketEncoder;
import org.pocketserver.server.net.netty.PocketHandler;
import org.pocketserver.server.net.netty.PocketWrapperEncoder;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.EpollDatagramChannel;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.DatagramChannel;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.util.AttributeKey;
import io.netty.util.internal.SystemPropertyUtil;

import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public final class PipelineUtil {
    public static final AttributeKey<InetSocketAddress> ADDRESS_ATTRIBUTE = AttributeKey.valueOf("recipient");
    public static final ChannelInitializer<DatagramChannel> INITIALIZER = new ChannelInitializer<DatagramChannel>() {
        @Override
        protected void initChannel(DatagramChannel ch) throws Exception {
            ch.pipeline().addLast("wrapper_encoder", new PocketWrapperEncoder());
            ch.pipeline().addLast("packet_decoder", new PocketDecoder());
            ch.pipeline().addLast("packet_encoder", new PocketEncoder());
            ch.pipeline().addLast("packet_handler", new PocketHandler());
        }
    };

    private static final boolean useEpoll = SystemPropertyUtil.get("os.name").trim().startsWith("linux");

    private PipelineUtil() {
        throw new UnsupportedOperationException("PipelineUtil cannot be instantiated!");
    }

    public static Class<? extends Channel> getChannelClass() {
        if (useEpoll) {
            return EpollDatagramChannel.class;
        } else {
            return NioDatagramChannel.class;
        }
    }

    public static EventLoopGroup newEventLoop(int numThreads) {
        ThreadFactory threadFactory = new ThreadFactory() {
            private final AtomicInteger counter = new AtomicInteger(1);

            @Override
            public Thread newThread(Runnable r) {
                return new Thread(r, String.format("Netty IO Thread #%1$d", counter.getAndIncrement()));
            }
        };
        ExecutorService executor = Executors.newFixedThreadPool(numThreads, threadFactory);
        if (useEpoll) {
            return new EpollEventLoopGroup(0, executor);
        } else {
            return new NioEventLoopGroup(0, executor);
        }
    }

    public static InetSocketAddress getAddress(ChannelHandlerContext ctx) {
        return ctx.attr(ADDRESS_ATTRIBUTE).get();
    }
}
