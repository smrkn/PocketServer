package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketPlayOutPlayerStatus extends Packet {
    private final Status status;

    public PacketPlayOutPlayerStatus(Status status) {
        super(PacketType.PLAYER_STATUS);
        this.status = status;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeInt(status.getId());
    }

    public enum Status {
        GOOD(0),
        SERVER_OUTDATED(1),
        CLIENT_OUTDATED(2),
        SPAWNED(3);

        private final int id;

        Status(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }
    }
}
