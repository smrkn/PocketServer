package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketRaknetOutConnectedPong extends Packet {
    private final long timestamp;

    public PacketRaknetOutConnectedPong(long timestamp) {
        super(PacketType.RAKNET_CONNECTED_PONG);
        this.timestamp = timestamp;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        long now = System.currentTimeMillis();
        buf.writeLong(timestamp);
        buf.writeLong(now);
    }
}
