package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketPlayOutDropItem extends Packet {

    public PacketPlayOutDropItem() {
        super(PacketType.DROP_ITEM);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {

    }
}
