package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.PacketType;

public class PacketRaknetOutNak extends AbstractRaknetAck {
    public PacketRaknetOutNak() {
        super(PacketType.RAKNET_NAK);
    }

    public PacketRaknetOutNak(int[] ids) {
        super(PacketType.RAKNET_NAK, ids);
    }
}
