package org.pocketserver.server.net.netty;

import org.pocketserver.server.PocketServer;
import org.pocketserver.server.net.MissingPacketException;
import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;
import org.pocketserver.server.net.PipelineUtil;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.socket.DatagramPacket;
import io.netty.handler.codec.MessageToMessageDecoder;

import org.slf4j.Logger;

import java.util.BitSet;
import java.util.List;

public class PocketDecoder extends MessageToMessageDecoder<DatagramPacket> {
    private static boolean dump = Boolean.getBoolean("pocket.hex-dump-inbound");

    @Override
    protected void decode(ChannelHandlerContext ctx, DatagramPacket msg, List<Object> out) throws Exception {
        ctx.attr(PipelineUtil.ADDRESS_ATTRIBUTE).set(msg.sender());
        ByteBuf content = msg.content();
        if (dump) {
            PocketServer.getInstance().getLogger().debug("Inbound: {}", ByteBufUtil.hexDump(content).toUpperCase());
        }

        byte packetId = content.readByte();
        Packet packet = PacketType.create(packetId, true);

        if ((packetId & 0xFF) > 126 && PocketServer.getInstance().getLogger().isTraceEnabled()) {
            BitSet set = BitSet.valueOf(new byte[]{packetId});

            boolean continuous = false;
            boolean pair = false;
            boolean nak = false;
            boolean ack;

            if (!(ack = set.get(6))) {
                if (!(nak = set.get(5))) {
                    pair = set.get(4);
                    continuous = set.get(3);
                }
            }

            Logger logger = PocketServer.getInstance().getLogger();
            logger.trace("User Defined Datagram Header");
            logger.trace("- isAck: {}", ack);
            logger.trace("- isNak: {}", nak);
            logger.trace("- isContinuous: {}", continuous);
            logger.trace("- isPair: {}", pair);

            if (ack) {
                assert packet.getType() == PacketType.RAKNET_ACK;
            }

            if (nak) {
                assert packet.getType() == PacketType.RAKNET_NAK;
            }
        }

        if (PocketServer.getInstance().getLogger().isDebugEnabled()) {
            PocketServer.getInstance().getLogger().debug("RECV {} from {}", packet.toString(), msg.sender());
        }
        packet.read(content);
        out.add(packet);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        if (cause.getCause() instanceof MissingPacketException) {
            MissingPacketException exception = (MissingPacketException) cause.getCause();
            PocketServer.getInstance().getLogger().error("TODO: Implement packet {}", String.format("0x%1$02X", exception.getPacketId()));
            return;
        }
        PocketServer.getInstance().getLogger().error("Failed to decode packet: ", cause);
    }
}
