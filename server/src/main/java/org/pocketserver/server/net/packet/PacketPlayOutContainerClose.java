package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketPlayOutContainerClose extends Packet {

    public PacketPlayOutContainerClose() {
        super(PacketType.CONTAINER_CLOSE);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {

    }
}
