package org.pocketserver.server.net;

import gnu.trove.impl.unmodifiable.TUnmodifiableByteObjectMap;
import gnu.trove.map.TByteObjectMap;
import gnu.trove.map.hash.TByteObjectHashMap;

public enum Reliability {
    UNRELIABLE(0x00),
    UNRELIABLE_SEQUENCED(0x01, true),
    RELIABLE(0x02),
    RELIABLE_ORDERED(0x03, true),
    RELIABLE_SEQUENCED(0x04, true),
    UNRELIABLE_ACK_RECEIPT(0x05, false),
    RELIABLE_ACK_RECEIPT(0x06, false),
    RELIABLE_ORDERED_ACK_RECEIPT(0x07, true);

    private static TByteObjectMap<Reliability> values;

    static {
        TByteObjectMap<Reliability> vals = new TByteObjectHashMap<>();
        for (Reliability reliability : values()) {
            vals.put(reliability.value, reliability);
        }
        values = new TUnmodifiableByteObjectMap<>(vals);
    }

    private final boolean ordered;
    private final byte value;

    Reliability(Number value) {
        this(value, false);
    }

    Reliability(Number value, boolean ordered) {
        this.value = value.byteValue();
        this.ordered = ordered;
    }

    public static Reliability of(byte value) {
        return values.get((byte) ((value & 0b011100000) >> 5));
    }

    public boolean isOrdered() {
        return ordered;
    }

    public byte asByte() {
        return value;
    }
}
