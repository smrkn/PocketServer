package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketPlayOutLevelEvent extends Packet {
    private short eventId;
    private float x;
    private float y;
    private float z;
    private int data;

    public PacketPlayOutLevelEvent() {
        super(PacketType.LEVEL_EVENT);
    }

    public PacketPlayOutLevelEvent(short eventId, float x, float y, float z, int data) {
        this();
        this.eventId = eventId;
        this.x = x;
        this.y = y;
        this.z = z;
        this.data = data;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeShort(eventId);
        buf.writeFloat(x);
        buf.writeFloat(y);
        buf.writeFloat(z);
        buf.writeInt(data);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        eventId = buf.readShort();
        x = buf.readFloat();
        y = buf.readFloat();
        z = buf.readFloat();
        data = buf.readInt();
    }
}
