package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketRaknetOutNoFreeIncomingConnections extends Packet {
    public PacketRaknetOutNoFreeIncomingConnections() {
        super(PacketType.RAKNET_NO_FREE_INCOMING_CONNECTION);
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        // NOP
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        // NOP
    }
}
