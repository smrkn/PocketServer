package org.pocketserver.server.net.packet;

import org.pocketserver.server.PocketServer;
import org.pocketserver.server.Session;
import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;
import org.pocketserver.server.net.PipelineUtil;
import org.pocketserver.server.player.PocketPlayer;
import org.pocketserver.server.world.PocketWorld;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.UUID;

public class PacketConnectInPlayerLogin extends Packet {
    private String username;
    private int protocol1;
    private int protocol2;
    private long clientId;
    private UUID clientUUID;
    private String address;
    private String secret;
    private Skin skin;

    public PacketConnectInPlayerLogin() {
        super(PacketType.PLAYER_LOGIN);
    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        PocketServer instance = PocketServer.getInstance();
        instance.getLogger().info("{} is logging in.", username);
        if (instance.getLogger().isDebugEnabled()) {
            instance.getLogger().debug("- Client UUID \"{}\"", clientUUID);
            instance.getLogger().debug("- Connected to \"{}\"", address);
            instance.getLogger().debug("- Protocol Version \"{}\"", protocol1);
            /*
            StringBuilder skinData = new StringBuilder("data:image/png;base64,");
            byte[] payload = Base64.getEncoder().encode(skin.texture);
            skinData.append(new String(payload, Charsets.UTF_8));

            PocketServer.getInstance().getLogger().debug("- Skin data {}", skinData.toString());
            */
        }
        final InetSocketAddress address = PipelineUtil.getAddress(ctx);
        instance.getExecutor().submit(() -> {
            Session session = instance.getSessions().get(address);
            if (session == null) {
                instance.getLogger().error("The session should have been created already. Exiting server.");
                instance.shutdown();
                return;
            }
            PocketWorld world = instance.getWorlds().get(0); //TODO Get the default world
            PocketPlayer player = new PocketPlayer(username, clientUUID, 10, session, world.getSpawn());
            session.setPlayer(player);
            session.send(new PacketPlayOutPlayerStatus(PacketPlayOutPlayerStatus.Status.GOOD), true);
            world.addPlayer(player);
        });
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        this.username = Packet.readString(buf);
        this.protocol1 = buf.readInt();
        this.protocol2 = buf.readInt();
        this.clientId = buf.readLong();
        this.clientUUID = Packet.readUUID(buf);
        this.address = Packet.readString(buf);
        this.secret = Packet.readString(buf);
        this.skin = new Skin(Packet.readString(buf), buf.readShort());
    }

    private static class Skin {
        boolean slim;
        String type;
        byte[] texture;

        public Skin(String s, short i) {
            //TODO: Parse the skin
        }
    }
}
