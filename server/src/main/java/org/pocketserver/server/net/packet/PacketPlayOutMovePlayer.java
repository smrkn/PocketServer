package org.pocketserver.server.net.packet;

import org.pocketserver.server.PocketServer;
import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;
import org.pocketserver.server.net.PipelineUtil;
import org.pocketserver.server.player.PocketPlayer;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.util.Attribute;

import com.google.common.base.MoreObjects;

import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class PacketPlayOutMovePlayer extends Packet {
    private long entityId;
    private float x;
    private float y;
    private float z;
    private float yaw;
    private float bodyYaw;
    private float pitch;
    private Mode mode;
    private boolean onGround;

    public PacketPlayOutMovePlayer() {
        super(PacketType.MOVE_PLAYER);
    }

    public PacketPlayOutMovePlayer(long entityId, float x, float y, float z, float yaw, float bodyYaw, float pitch, Mode mode, boolean onGround) {
        this();
        this.entityId = entityId;
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.bodyYaw = bodyYaw;
        this.pitch = pitch;
        this.mode = mode;
        this.onGround = onGround;
    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        Attribute<InetSocketAddress> attr = ctx.attr(PipelineUtil.ADDRESS_ATTRIBUTE);
        Optional<PocketPlayer> optional = PocketServer.getInstance().getPlayer(attr.get());
        if (!optional.isPresent()) {
            PocketServer.getInstance().getLogger().debug("No player found for address {}", attr.get());
        }
        PocketPlayer player = optional.get();
        if (player.getEntityId() != entityId) {
            PocketServer.getInstance().getLogger().debug("Possible spoof for player from address {}", attr.get());
        }
        PocketServer.getInstance().getLogger().debug(MoreObjects.toStringHelper(this)
                .add("x", x)
                .add("y", y)
                .add("z", z)
                .add("pitch", pitch)
                .add("yaw", yaw)
                .add("bodyYaw", bodyYaw)
                .add("mode", mode)
                .add("onGround", onGround)
                .toString()
        );
        PocketServer.getInstance().broadcast(this, p -> p != player); //TODO: Update player location
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeLong(entityId);
        buf.writeFloat(x);
        buf.writeFloat(y);
        buf.writeFloat(z);
        buf.writeFloat(yaw);
        buf.writeFloat(bodyYaw);
        buf.writeFloat(pitch);
        buf.writeByte(mode.getId());
        buf.writeByte((onGround ? 1 : 0));
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        this.entityId = buf.readLong();
        this.x = buf.readFloat();
        this.y = buf.readFloat();
        this.z = buf.readFloat();
        this.yaw = buf.readFloat();
        this.bodyYaw = buf.readFloat();
        this.pitch = buf.readFloat();
        this.mode = Mode.byId(buf.readByte());
        this.onGround = (buf.readByte() != 0);
    }

    public enum Mode {
        NORMAL(0),
        RESET(1),
        ROTATION(2);

        private static final Map<Integer, Mode> idMap;

        static {
            idMap = Collections.unmodifiableMap(new HashMap<Integer, Mode>() {{
                Arrays.asList(Mode.values()).forEach(mode -> put(mode.getId(), mode));
            }});
        }

        private final int id;

        Mode(int id) {
            this.id = id;
        }

        public static Mode byId(int id) {
            return idMap.get(id);
        }

        public int getId() {
            return id;
        }
    }
}
