package org.pocketserver.server.net;

import org.pocketserver.server.Session;

import java.net.InetSocketAddress;
import java.util.function.Consumer;

public final class PacketWrapper {
    private final InetSocketAddress recipient;
    private final Packet packet;
    private final Consumer<Session> ackReceipt;

    public PacketWrapper(InetSocketAddress recipient, Packet packet, Consumer<Session> ackReceipt) {
        this.recipient = recipient;
        this.packet = packet;
        this.ackReceipt = ackReceipt;
    }

    public InetSocketAddress getRecipient() {
        return recipient;
    }

    public Packet getPacket() {
        return packet;
    }

    public Consumer<Session> getAckReceipt() {
        return ackReceipt;
    }
}
