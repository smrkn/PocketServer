package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketRaknetOutConnectionBanned extends Packet {
    public PacketRaknetOutConnectionBanned() {
        super(PacketType.RAKNET_CONNECTION_BANNED);
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        // NOP
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        // NOP
    }
}
