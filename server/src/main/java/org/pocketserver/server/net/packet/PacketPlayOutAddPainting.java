package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketPlayOutAddPainting extends Packet {
    private long entityId;
    private int x;
    private int y;
    private int z;
    private int direction;
    private String title;

    public PacketPlayOutAddPainting() {
        super(PacketType.ADD_PAINTING);
    }

    public PacketPlayOutAddPainting(long entityId, int x, int y, int z, int direction, String title) {
        this();
        this.entityId = entityId;
        this.x = x;
        this.y = y;
        this.z = z;
        this.direction = direction;
        this.title = title;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeLong(entityId);
        buf.writeInt(x);
        buf.writeInt(y);
        buf.writeInt(z);
        buf.writeInt(direction);
        writeString(buf, title);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        entityId = buf.readLong();
        x = buf.readInt();
        y = buf.readInt();
        z = buf.readInt();
        direction = buf.readInt();
        title = readString(buf);
    }
}
