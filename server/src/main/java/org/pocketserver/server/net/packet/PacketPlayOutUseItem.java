package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

import java.util.List;

public class PacketPlayOutUseItem extends Packet {

    public PacketPlayOutUseItem() {
        super(PacketType.USE_ITEM);
    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {

    }

    @Override
    public void write(ByteBuf buf) throws Exception {

    }

    @Override
    public void read(ByteBuf buf) throws Exception {

    }
}
