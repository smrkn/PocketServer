package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

import java.util.List;

public class PacketRaknetInUnconnectedPing extends Packet {
    private long identifier;

    public PacketRaknetInUnconnectedPing() {
        super(PacketType.RAKNET_UNCONNECTED_PING);
    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        out.add(new PacketRaknetOutUnconnectedPong(this.identifier));
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        this.identifier = buf.readLong();
    }
}
