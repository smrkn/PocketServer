package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.ChannelHandlerContext;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class PacketPlayOutBatch extends Packet {
    private byte[] compressed = null;
    private Packet decompressed;

    public PacketPlayOutBatch() {
        super(PacketType.BATCH);
    }

    public PacketPlayOutBatch(ByteBufAllocator allocator, Packet packet) throws Exception {
        super(PacketType.BATCH);

        ByteBuf packetBuffer = allocator.buffer();
        {
            ByteBuf internal = allocator.buffer();
            internal.writeByte(packet.getType().getId());
            packet.write(internal);

            packetBuffer.writeInt(internal.readableBytes());
            packetBuffer.writeBytes(internal);
            internal.release();
        }
        byte[] packetData = new byte[packetBuffer.readableBytes()];
        packetBuffer.readBytes(packetData);
        packetBuffer.release();

        Deflater deflater = new Deflater();
        deflater.setInput(packetData);
        deflater.finish();

        try (ByteArrayOutputStream stream = new ByteArrayOutputStream(packetData.length)) {
            byte[] bytes = new byte[1024];
            while (!deflater.finished()) {
                int bytesCompressed = deflater.deflate(bytes);
                stream.write(bytes, 0, bytesCompressed);
            }
            this.compressed = stream.toByteArray();
        }
    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        if (decompressed != null) {
            decompressed.handle(ctx, out);
        }
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeInt(compressed.length);
        buf.writeBytes(compressed);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        byte[] payload = new byte[buf.readInt()];
        buf.readBytes(payload);

        ByteBuf decompressed = buf.alloc().buffer();
        try {
            Inflater inflater = new Inflater();
            inflater.setInput(payload);

            byte[] buffer = new byte[1024];
            while (!inflater.finished()) {
                int amount = inflater.inflate(buffer);
                decompressed.writeBytes(buffer, 0, amount);
            }

            Packet packet = PacketType.create(decompressed.readByte(), false);
            packet.read(decompressed);
            this.decompressed = packet;
        } finally {
            decompressed.release();
        }
    }
}
