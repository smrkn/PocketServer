package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;

import java.net.InetAddress;
import java.net.InetSocketAddress;

public class PacketRaknetOutOpenConnectionRequestAccepted extends Packet {
    private static final InetSocketAddress LOOPBACK = new InetSocketAddress(InetAddress.getLoopbackAddress(), 19132);
    private static final InetSocketAddress SYSTEM = new InetSocketAddress("0.0.0.0", 19132);
    private final InetSocketAddress clientAddress;
    private final long clientTimestamp;
    private final long serverTimestamp;

    public PacketRaknetOutOpenConnectionRequestAccepted(InetSocketAddress clientAddress, long clientTimestamp) {
        super(PacketType.RAKNET_OPEN_CONNECTION_REQUEST_ACCEPTED);
        this.clientAddress = clientAddress;
        this.clientTimestamp = clientTimestamp;
        this.serverTimestamp = System.currentTimeMillis();
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        writeAddress(buf, clientAddress);
        writeAddress(buf, LOOPBACK);
        for (int i = 0; i < 9; i++) {
            writeAddress(buf, SYSTEM);
        }
        buf.writeLong(clientTimestamp);
        buf.writeLong(serverTimestamp);
    }
}
