package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketPlayOutSetHealth extends Packet {
    private final int health;

    public PacketPlayOutSetHealth(int health) {
        super(PacketType.SET_HEALTH);
        this.health = health;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeInt(health);
    }
}
