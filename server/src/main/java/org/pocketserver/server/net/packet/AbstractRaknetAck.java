package org.pocketserver.server.net.packet;

import org.pocketserver.server.PocketServer;
import org.pocketserver.server.Session;
import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;
import org.pocketserver.server.net.PipelineUtil;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;

class AbstractRaknetAck extends Packet {
    protected int[] packets;

    protected AbstractRaknetAck(PacketType type) {
        this(type, null);
    }

    protected AbstractRaknetAck(PacketType type, int[] packets) {
        super(type);
        this.packets = packets;
    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        PocketServer.getInstance().getLogger().debug("{} {}ed the packets {}", PipelineUtil.getAddress(ctx), getType(), Arrays.toString(packets));
        Session session = PocketServer.getInstance().getSessions().get(ctx.attr(PipelineUtil.ADDRESS_ATTRIBUTE).get());
        if (session == null) {
            return; //Already accepted receipt.
        }
        for (int id : packets) {
            Consumer<Session> receipt = session.getReceipt(id);
            if (receipt != null) {
                receipt.accept(session);
            }
        }
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        if (this.packets == null || this.packets.length < 1) {
            throw new IllegalArgumentException("invalid number of packets");
        }

        ByteBuf payload = buf.alloc().buffer();

        try {
            int count = packets.length;
            int previous = packets[0];
            int start = packets[0];
            int records = 0;

            for (int cursor = 1; cursor < count; cursor++) {
                int current = packets[cursor];
                int diff = current - previous;
                if (diff == 1) {
                    previous = current;
                } else if (diff > 1) {
                    boolean single = start == previous;
                    payload.writeBoolean(single);
                    Packet.writeInt24(payload, start);
                    if (!single) {
                        Packet.writeInt24(payload, previous);
                    }
                    previous = current;
                    start = current;
                    records++;
                }
            }

            boolean single = start == previous;
            payload.writeBoolean(single);
            Packet.writeInt24(payload, start);
            if (!single) {
                Packet.writeInt24(payload, previous);
            }
            records++;

            buf.writeShort(records);
            buf.writeBytes(payload);
        } finally {
            payload.release();
        }
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        int records = buf.readUnsignedShort();
        TIntList list = new TIntArrayList();
        for (int i = 0; i < records && i < 4096; i++) {
            if (buf.readBoolean()) {
                list.add(Packet.readInt24(buf));
            } else {
                int start = Packet.readInt24(buf);
                int end = Packet.readInt24(buf);
                if ((end - start) > 512) {
                    end = start + 512;
                }
                for (int cursor = start; cursor <= end; ++cursor) {
                    list.add(cursor);
                }
            }
        }
        this.packets = list.toArray();
    }
}
