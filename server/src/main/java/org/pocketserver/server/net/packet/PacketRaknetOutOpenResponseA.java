package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;
import org.pocketserver.server.net.ProtocolConstants;

import io.netty.buffer.ByteBuf;

public class PacketRaknetOutOpenResponseA extends Packet {
    private final int mtu;

    public PacketRaknetOutOpenResponseA(int mtu) {
        super(PacketType.RAKNET_OPEN_RESPONSE_A);
        this.mtu = mtu;
    }

    @Override
    public void write(ByteBuf buf) {
        writeOfflineMessageId(buf);
        buf.writeLong(ProtocolConstants.SERVER_ID);
        buf.writeByte(0);
        buf.writeShort(mtu);
    }
}
