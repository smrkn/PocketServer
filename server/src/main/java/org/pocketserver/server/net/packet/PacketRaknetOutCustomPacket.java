package org.pocketserver.server.net.packet;

import org.pocketserver.server.PocketServer;
import org.pocketserver.server.Session;
import org.pocketserver.server.net.MissingPacketException;
import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;
import org.pocketserver.server.net.PacketWrapper;
import org.pocketserver.server.net.PipelineUtil;
import org.pocketserver.server.net.Reliability;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.InetSocketAddress;
import java.util.List;

public class PacketRaknetOutCustomPacket extends Packet {
    private final List<Packet> encapsulated;
    private int sequenceNumber;

    public PacketRaknetOutCustomPacket() {
        super(PacketType.RAKNET_CUSTOM_PACKET);

        if (PocketServer.getInstance().getLogger().isDebugEnabled()) {
            // noinspection unchecked
            this.encapsulated = (List<Packet>) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{List.class}, new InvocationHandler() {
                private final List<Packet> delegate = Lists.newArrayList();

                @Override
                public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                    if (method.getName().equals("add")) {
                        Packet packet = Packet.class.cast(args[0]);
                        PocketServer.getInstance().getLogger().debug("Decoded {}", packet.toString());
                    }
                    return method.invoke(delegate, args);
                }
            });
        } else {
            this.encapsulated = Lists.newArrayList();
        }
    }

    public static List<OutgoingCustomPacketBuffer> writeMany(ChannelHandlerContext ctx, PacketWrapper msg) throws Exception {
        InetSocketAddress address = PipelineUtil.getAddress(ctx);
        Session session = PocketServer.getInstance().getSessions().get(address);

        if (session == null) {
            throw new IllegalStateException("Session has not been established");
        }

        ByteBuf body = ctx.alloc().buffer();
        Packet packet = msg.getPacket();
        byte id = packet.getType().getId();
        if ((id & 0xFF) > 0x8E) {
            body.writeByte(0x8E);
        }
        body.writeByte(id);
        packet.write(body);

        int count = (int) Math.ceil(body.readableBytes() / (session.getMtu() - 60));
        boolean multiple = count > 1;

        ByteBuf header = ctx.alloc().buffer();
        header.writeByte(multiple ? 0x8C : 0x84);
        int sequenceNumber = session.getNextDatagramSequenceNumber();
        Packet.writeInt24(header, sequenceNumber);

        /*
         * TODO: Disassemble .so and find what makes a packet "reliable"
         */
        Reliability reliability = Reliability.RELIABLE;
        header.writeByte((reliability.asByte() << 5) | (multiple ? 0b00010000 : 0x00));
        header.writeShort(body.readableBytes() * 8);

        //noinspection ConstantConditions
        if (reliability == Reliability.RELIABLE || reliability == Reliability.RELIABLE_SEQUENCED || reliability == Reliability.RELIABLE_ACK_RECEIPT || reliability == Reliability.RELIABLE_ORDERED || reliability == Reliability.RELIABLE_ORDERED_ACK_RECEIPT) {
            Packet.writeInt24(header, session.getNextMessageNumber());
        }

        if (reliability.isOrdered()) {
            Packet.writeInt24(header, 0); // REPLACE: orderingIndex
            header.writeByte(0); // REPLACE: orderingChannel
        }

        if (multiple) {
            header.writeInt(count);
            header.writeShort(0); // REPLACE: splitPacketId
            header.writeInt(0); // REPLACE: splitPacketIndex
        }

        if (!multiple) {
            ByteBuf full = ctx.alloc().buffer();
            full.writeBytes(header);
            full.writeBytes(body);
            header.release();
            body.release();
            return ImmutableList.of(new OutgoingCustomPacketBuffer(full, sequenceNumber));
        } else {
            /*
             * TODO: Create individual packet headers and concat buffers
             */
            return ImmutableList.of();
        }
    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        out.add(new PacketRaknetOutAck(new int[]{sequenceNumber}));
        for (Packet packet : encapsulated) {
            packet.handle(ctx, out);
        }
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        this.sequenceNumber = Packet.readInt24(buf);
        PocketServer.getInstance().getLogger().debug("datagramSequenceNumber: {}", sequenceNumber);

        int reliableMessageNumber = -1;
        int orderingChannel = 0;
        int sequenceIndex = -1;
        int orderingIndex = 0;

        int splitPacketIndex = -1;
        int splitPacketId = -1;
        int splitPackets = -1;

        while (buf.isReadable(4)) {
            byte packetFlags = buf.readByte();
            int packetLength = buf.readShort() / 8;
            if (packetLength <= 0) {
                PocketServer.getInstance().getLogger().debug("- header: {}", String.format("%8s", Integer.toBinaryString(packetFlags & 0xFF)).replace(' ', '0'));
                PocketServer.getInstance().getLogger().debug("- length: {}", packetLength);
                PocketServer.getInstance().getLogger().debug("- remaining: {}", buf.readableBytes());
                PocketServer.getInstance().getLogger().debug("Stopping custom packet decoding due to length <= 0");
                break;
            }

            Reliability reliability = Reliability.of(packetFlags);
            boolean split = (packetFlags & 0b00010000) > 0;

            if (PocketServer.getInstance().getLogger().isDebugEnabled()) {
                PocketServer.getInstance().getLogger().debug("- header: {}", String.format("%8s", Integer.toBinaryString(packetFlags & 0xFF)).replace(' ', '0'));
                PocketServer.getInstance().getLogger().debug("- length: {}", packetLength);
                PocketServer.getInstance().getLogger().debug("- reliability: {}", reliability);
                PocketServer.getInstance().getLogger().debug("- split: {}", split);
            }

            if (reliability == Reliability.RELIABLE || reliability == Reliability.RELIABLE_ORDERED || reliability == Reliability.RELIABLE_SEQUENCED) {
                reliableMessageNumber = Packet.readInt24(buf);
                PocketServer.getInstance().getLogger().debug("- reliableMessageNumber: {}", reliableMessageNumber);
            }

            if (reliability == Reliability.UNRELIABLE_SEQUENCED || reliability == Reliability.RELIABLE_SEQUENCED) {
                sequenceIndex = Packet.readInt24(buf);
                PocketServer.getInstance().getLogger().debug("- sequenceIndex: {}", sequenceIndex);
            }

            if (reliability.isOrdered()) {
                orderingIndex = Packet.readInt24(buf);
                orderingChannel = buf.readByte();
                PocketServer.getInstance().getLogger().debug("- orderingChannel: {}", orderingChannel);
                PocketServer.getInstance().getLogger().debug("- orderingIndex: {}", orderingIndex);
            }

            if (split) {
                splitPackets = buf.readInt();
                splitPacketId = buf.readShort();
                splitPacketIndex = buf.readInt();

                PocketServer.getInstance().getLogger().debug("- splitPacketCount: {}", splitPackets);
                PocketServer.getInstance().getLogger().debug("- splitPacketId: {}", splitPacketId);
                PocketServer.getInstance().getLogger().debug("- splitPacketIndex: {}", splitPacketIndex);
            }

            ByteBuf content = buf.alloc().buffer(packetLength, packetLength);
            buf.readBytes(content, 0, packetLength);
            content.writerIndex(packetLength);

            byte packetId = content.readByte();
            if (packetId == (byte) 0x8E) {
                packetId = content.readByte();
                PocketServer.getInstance().getLogger().debug("- actual packet id: {}", String.format("0x%02x", packetId));
            }

            if (split) {
                PocketServer.getInstance().getLogger().debug("TODO: Implement split packets, they're now necessary.");
                buf.skipBytes(packetLength);
                continue;
            }

            try {
                Packet packet = PacketType.create(packetId, false);
                packet.read(content);
                encapsulated.add(packet);
            } catch (MissingPacketException ignored1) {
                try {
                    Packet packet = PacketType.create(packetId, true);
                    packet.read(content);
                    encapsulated.add(packet);
                } catch (MissingPacketException ignored2) {
                    PocketServer.getInstance().getLogger().error("TODO: Implement {}", String.format("0x%1$02X", packetId));
                    buf.skipBytes(packetLength);
                }
            } finally {
                content.release();
            }
        }
    }

    public static class OutgoingCustomPacketBuffer {
        private final ByteBuf buf;
        private final int sequenceNumber;

        public OutgoingCustomPacketBuffer(ByteBuf buf, int sequenceNumber) {
            this.buf = buf;
            this.sequenceNumber = sequenceNumber;
        }

        public ByteBuf getBuffer() {
            return buf;
        }

        public int getSequenceNumber() {
            return sequenceNumber;
        }
    }
}
