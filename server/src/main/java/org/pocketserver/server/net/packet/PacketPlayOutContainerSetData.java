package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketPlayOutContainerSetData extends Packet {

    public PacketPlayOutContainerSetData() {
        super(PacketType.CONTAINER_SET_DATA);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {

    }
}
