package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketPlayOutEntityEvent extends Packet {
    private long entityId;
    private byte eventId;

    public PacketPlayOutEntityEvent() {
        super(PacketType.ENTITY_EVENT);
    }

    public PacketPlayOutEntityEvent(long entityId, byte eventId) {
        this();
        this.entityId = entityId;
        this.eventId = eventId;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeLong(entityId);
        buf.writeByte(eventId);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        entityId = buf.readLong();
        eventId = buf.readByte();
    }
}
