package org.pocketserver.server.net.packet;

import org.pocketserver.server.PocketServer;
import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;
import org.pocketserver.server.player.PocketPlayer;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

import java.util.List;
import java.util.Optional;

public class PacketPlayOutText extends Packet {
    private TextType type;
    private String message;
    private Optional<String> source;

    public PacketPlayOutText(TextType type, String message, Optional<String> source) {
        this();
        this.type = type;
        this.message = message;
        this.source = source;
    }

    public PacketPlayOutText(TextType type, String message) {
        this(type, message, Optional.empty());
    }

    public PacketPlayOutText() {
        super(PacketType.TEXT);
        this.source = Optional.empty();
    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        PocketServer server = PocketServer.getInstance();
        if (type == TextType.CHAT) {
            Optional<PocketPlayer> optional = server.getPlayer(source.orElseThrow(NullPointerException::new));
            if (!optional.isPresent()) {
                return;
            }
            PocketPlayer player = optional.get();
            if (!player.getName().equals(source.get())) {
                server.getLogger().debug("{} possibly spoofing chat packets with name {}.", player, source.get());
                return;
            }
            player.chat(message);
        } else {
            server.getLogger().warn("Received an unimplemented %s text type from client.", type.name());
        }
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeByte(type.id);
        if (type.requiresSource()) {
            writeString(buf, source.orElse(""));
        }
        writeString(buf, message);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        byte id = buf.readByte();
        this.type = TextType.parseId(id);
        if (type.requiresSource()) {
            this.source = Optional.of(readString(buf));
        }
        this.message = readString(buf);
    }

    public enum TextType {
        RAW(0),
        CHAT(1),
        TRANSLATION(2),
        POPUP(3),
        TIP(4),
        SYSTEM(5);

        private final byte id;

        TextType(byte id) {
            this.id = id;
        }

        TextType(int i) {
            this((byte) i);
        }

        private static TextType parseId(byte id) {
            return values()[id];
        }

        public boolean requiresSource() {
            return this == CHAT || this == POPUP;
        }
    }
}
