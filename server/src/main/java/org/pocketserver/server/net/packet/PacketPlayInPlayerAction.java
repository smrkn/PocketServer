package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketPlayInPlayerAction extends Packet {
    private long entityId;
    private Action action;
    private int x;
    private int y;
    private int z;
    private int face;

    public PacketPlayInPlayerAction() {
        super(PacketType.PLAYER_ACTION);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        entityId = buf.readLong();
        action = Action.getId(buf.readInt());
        x = buf.readInt();
        y = buf.readInt();
        z = buf.readInt();
        face = buf.readInt();
    }

    public enum Action {
        START_BREAK(0),
        ABORT_BREAK(1),
        STOP_BREAK(2),
        RELEASE_ITEM(5),
        STOP_SLEEPING(6),
        RESPAWN(7),
        JUMP(8),
        START_SPRINT(9),
        STOP_SPRINT(10),
        START_SNEAK(11),
        STOP_SNEAK(12),
        DIMENSION_CHANGE(13);

        private final int id;

        Action(int id) {
            this.id = id;
        }

        public static Action getId(int id) {
            return values()[id > 2 ? id - 2 : id];
        }

        public int getId() {
            return id;
        }
    }
}
