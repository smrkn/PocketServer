package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

import java.util.List;

public class PacketRaknetInConnectedPing extends Packet {
    private long timestamp;

    public PacketRaknetInConnectedPing() {
        super(PacketType.RAKNET_CONNECTED_PING);
    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        out.add(new PacketRaknetOutConnectedPong(timestamp));
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        this.timestamp = buf.readLong();
    }
}
