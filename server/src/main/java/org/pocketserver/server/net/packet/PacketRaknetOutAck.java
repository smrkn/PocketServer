package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.PacketType;

public class PacketRaknetOutAck extends AbstractRaknetAck {
    public PacketRaknetOutAck() {
        super(PacketType.RAKNET_ACK);
    }

    public PacketRaknetOutAck(int[] ids) {
        super(PacketType.RAKNET_ACK, ids);
    }
}
