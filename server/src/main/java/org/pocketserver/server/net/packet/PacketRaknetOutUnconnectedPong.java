package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;
import org.pocketserver.server.net.ProtocolConstants;

import io.netty.buffer.ByteBuf;

public class PacketRaknetOutUnconnectedPong extends Packet {
    private long identifier;

    public PacketRaknetOutUnconnectedPong(long identifier) {
        super(PacketType.RAKNET_UNCONNECTED_PONG);
        this.identifier = identifier;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeLong(this.identifier);
        buf.writeLong(ProtocolConstants.SERVER_ID);
        Packet.writeOfflineMessageId(buf);
        StringBuilder builder = new StringBuilder("MCPE;");
        {
            builder.append("A PocketServer MCPE Server.").append(";");
            builder.append(ProtocolConstants.PROTOCOL_VERSION).append(";");
            builder.append("0.14.2").append(";");
            builder.append(0).append(";");
            builder.append(20);
        }
        Packet.writeString(buf, builder.toString());
    }
}
