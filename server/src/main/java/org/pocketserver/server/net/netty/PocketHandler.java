package org.pocketserver.server.net.netty;

import org.pocketserver.server.PocketServer;
import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PipelineUtil;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import com.google.common.collect.Lists;

import java.net.InetSocketAddress;
import java.util.List;

public class PocketHandler extends SimpleChannelInboundHandler<Packet> {
    @Override
    protected void messageReceived(ChannelHandlerContext ctx, Packet msg) throws Exception {
        InetSocketAddress address = PipelineUtil.getAddress(ctx);
        try {
            List<Packet> outbound = Lists.newArrayList();
            msg.handle(ctx, outbound);

            if (!outbound.isEmpty()) {
                for (Packet packet : outbound) {
                    ChannelFuture future = ctx.write(packet);
                    if (PocketServer.getInstance().getLogger().isDebugEnabled()) {
                        future.addListener(channelFuture -> {
                            if (channelFuture.isSuccess()) {
                                PocketServer.getInstance().getLogger().debug("SEND {} to {}", packet.toString(), address);
                            }
                        });
                    }
                }
                ctx.flush();
            }
        } finally {
            ctx.attr(PipelineUtil.ADDRESS_ATTRIBUTE).remove();
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        PocketServer.getInstance().getLogger().error("An error occurred whilst handling a packet: ", cause);
        cause.printStackTrace();
    }
}
