package org.pocketserver.server.net;

public final class MissingPacketException extends IllegalArgumentException {
    private final byte packetId;

    public MissingPacketException(byte packetId) {
        super(String.format("A packet with the id 0x%1$02X does not exist", packetId));
        this.packetId = packetId;
    }

    public byte getPacketId() {
        return packetId;
    }
}
