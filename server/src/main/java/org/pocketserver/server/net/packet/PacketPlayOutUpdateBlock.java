package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketPlayOutUpdateBlock extends Packet {
    private static final byte FLAG_ALL_PRIORITY = (0b0001 | 0b0010) | 0b1000;
    private BlockRecord[] records;

    public PacketPlayOutUpdateBlock() {
        super(PacketType.UPDATE_BLOCK);
    }

    public PacketPlayOutUpdateBlock(BlockRecord... records) {
        super(PacketType.UPDATE_BLOCK);
        this.records = records;
    }

    @Override
    public void write(ByteBuf buf) throws Exception {
        buf.writeInt(records.length);
        for (BlockRecord record : records) {
            buf.writeInt(record.x);
            buf.writeInt(record.z);
            buf.writeByte(record.y);
            buf.writeByte(record.id);
            buf.writeByte((record.flags) << 4 | record.data);
        }
    }

    public static class BlockRecord {
        private final int x;
        private final int z;
        private final byte y;
        private final byte id;
        private final byte data;
        private final byte flags;

        BlockRecord(int x, int z, byte y, byte id, byte data) {
            this.x = x;
            this.z = z;
            this.y = y;
            this.id = id;
            this.data = data;
            this.flags = FLAG_ALL_PRIORITY;
        }
    }
}
