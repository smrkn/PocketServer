package org.pocketserver.server.net.netty;

import org.pocketserver.server.PocketServer;
import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PipelineUtil;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.socket.DatagramPacket;
import io.netty.handler.codec.MessageToMessageEncoder;

import java.net.InetSocketAddress;
import java.util.List;

public class PocketEncoder extends MessageToMessageEncoder<Packet> {
    static boolean dump = Boolean.getBoolean("pocket.hex-dump-outbound");

    @Override
    protected void encode(ChannelHandlerContext ctx, Packet msg, List<Object> out) throws Exception {
        InetSocketAddress recipient = PipelineUtil.getAddress(ctx);
        ByteBuf buf = ctx.alloc().buffer();
        buf.writeByte(msg.getType().getId() & 0xFF);
        msg.write(buf);
        if (dump) {
            PocketServer.getInstance().getLogger().debug("Outbound: {}", ByteBufUtil.hexDump(buf).toUpperCase());
        }
        out.add(new DatagramPacket(buf, recipient));
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        PocketServer.getInstance().getLogger().error("Failed to encode packet: ", cause);
    }
}
