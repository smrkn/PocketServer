package org.pocketserver.server.net.packet;

import org.pocketserver.server.PocketServer;
import org.pocketserver.server.Session;
import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;
import org.pocketserver.server.net.PipelineUtil;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

import java.net.InetSocketAddress;
import java.util.List;

public class PacketRaknetInOpenConnectionRequest extends Packet {
    private long timestamp;
    private long clientId;
    private boolean secure;

    public PacketRaknetInOpenConnectionRequest() {
        super(PacketType.RAKNET_OPEN_CONNECTION_REQUEST);
    }

    @Override
    public void handle(ChannelHandlerContext ctx, List<Packet> out) throws Exception {
        InetSocketAddress address = PipelineUtil.getAddress(ctx);
        Session session = PocketServer.getInstance().getSessions().get(address);
        if (session == null) {
            PocketServer.getInstance().getLogger().error("Session for {} should not be null", address);
            return;
        }
        session.send(new PacketRaknetOutOpenConnectionRequestAccepted(address, timestamp), true);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {
        clientId = buf.readLong();
        timestamp = buf.readLong();
        secure = buf.readBoolean();
    }
}
