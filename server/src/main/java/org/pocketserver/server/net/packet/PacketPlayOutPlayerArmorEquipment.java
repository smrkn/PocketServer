package org.pocketserver.server.net.packet;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketPlayOutPlayerArmorEquipment extends Packet {

    public PacketPlayOutPlayerArmorEquipment() {
        super(PacketType.PLAYER_ARMOR_EQUIPMENT);
    }

    @Override
    public void read(ByteBuf buf) throws Exception {

    }
}
