package org.pocketserver.server;

import org.pocketserver.server.net.Packet;
import org.pocketserver.server.net.PacketWrapper;
import org.pocketserver.server.net.packet.PacketPlayOutBatch;
import org.pocketserver.server.player.PocketPlayer;

import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;

import com.google.common.base.Preconditions;
import com.google.common.collect.Queues;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import java.net.InetSocketAddress;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

public final class Session implements Iterable<PacketWrapper> {
    private static final ListeningExecutorService SERVICE = MoreExecutors.listeningDecorator(Executors.newSingleThreadExecutor());
    private final Queue<PacketWrapper> packetQueue;
    private final AtomicInteger datagramSequenceNumber;
    private final AtomicInteger messageNumber;
    private final InetSocketAddress address;
    private final Channel channel;
    private final Map<Integer, Consumer<Session>> receipts;

    private PocketPlayer player;
    private short mtu = -1;
    private UUID clientId;
    private State state;

    public Session(Channel channel, InetSocketAddress address) {
        this.packetQueue = Queues.newConcurrentLinkedQueue();
        this.datagramSequenceNumber = new AtomicInteger();
        this.messageNumber = new AtomicInteger();
        this.state = State.NOT_CONNECTED;
        this.address = address;
        this.channel = channel;
        this.receipts = new ConcurrentHashMap<>();

        PocketServer.getInstance().getLogger().debug("Created session for {}", address);
    }

    public void send(Packet packet) {
        send(packet, false);
    }

    public void send(Packet packet, boolean immediate) {
        send(packet, immediate, null);
    }

    public void send(Packet packet, boolean immediate, Consumer<Session> runnable) {
        Preconditions.checkNotNull(packet, "packet");
        PacketWrapper wrapper = new PacketWrapper(address, packet, runnable);
        if (immediate) {
            Runnable task = () -> {
                ChannelFuture future = channel.writeAndFlush(wrapper);
                if (PocketServer.getInstance().getLogger().isDebugEnabled()) {
                    future.addListener(channelFuture -> {
                        if (channelFuture.isSuccess()) {
                            PocketServer.getInstance().getLogger().debug("SEND {} to {}", packet.toString(), address);
                        }
                    });
                }
            };
            if (channel.eventLoop().inEventLoop()) {
                task.run();
            } else {
                channel.eventLoop().submit(task);
            }
            return;
        }
        packetQueue.offer(wrapper);
    }

    public void compressPacket(ByteBufAllocator allocator, Packet packet, Consumer<PacketPlayOutBatch> consumer) {
        ListenableFuture<PacketPlayOutBatch> future = SERVICE.submit(() -> new PacketPlayOutBatch(allocator, packet));
        future.addListener(() -> {
            try {
                PacketPlayOutBatch batch = future.get();
                if (batch != null) {
                    consumer.accept(batch);
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }, SERVICE);
    }

    public UUID getClientId() {
        return clientId;
    }

    public void setClientId(UUID clientId) {
        this.clientId = clientId;
    }

    public short getMtu() {
        return mtu;
    }

    public void setMtu(short mtu) {
        this.mtu = mtu;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public int getNextDatagramSequenceNumber() {
        return increment(datagramSequenceNumber);
    }

    public int getNextMessageNumber() {
        return increment(messageNumber);
    }

    public PocketPlayer getPlayer() {
        return player;
    }

    public void setPlayer(PocketPlayer player) {
        this.player = player;
    }

    public void clearPacketQueue() {
        this.packetQueue.clear();
    }

    public boolean hasPacketQueued() {
        return !packetQueue.isEmpty();
    }

    public void addAckReceipt(int ackId, Consumer<Session> runnable) {
        this.receipts.put(ackId, runnable);
    }

    public Consumer<Session> getReceipt(int ackId) {
        return this.receipts.getOrDefault(ackId, null);
    }

    @Override
    public int hashCode() {
        return Objects.hash(address.getPort(), address.getAddress());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != Session.class) {
            return false;
        }

        if (obj == this) {
            return true;
        }

        Session that = (Session) obj;
        return that.address.getPort() == address.getPort() && that.address.getAddress().equals(address.getAddress());
    }

    private int increment(AtomicInteger ref) {
        return ref.getAndAccumulate(1, (left, right) -> {
            int sum = left + right;
            if (sum > 0x00FFFFFF) {
                sum = 0;
            }
            return sum;
        });
    }

    @Override
    public Iterator<PacketWrapper> iterator() {
        return packetQueue.iterator();
    }

    public enum State {
        NOT_CONNECTED,
        CONNECTING,
        CONNECTED
    }
}
