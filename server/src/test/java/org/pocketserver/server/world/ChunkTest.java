package org.pocketserver.server.world;

import org.pocketserver.server.block.Material;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ChunkTest {
    @Test
    public void testBlockOrdering() {
        PocketChunk chunk = new PocketChunk(0, 0);
        chunk.setBlock(0, 0, 1, Material.DIRT);
        chunk.setBlock(5, 2, 5, Material.COBBLESTONE);
        Assert.assertEquals(Material.DIRT, chunk.getBlock(0, 0, 1).getMaterial());
        Assert.assertEquals(Material.COBBLESTONE, chunk.getBlock(5, 2, 5).getMaterial());
    }


    @Test
    public void testInitialChunk() {
        PocketChunk chunk = new PocketChunk(0, 0);
        for (int x = 0; x < 16; x++) {
            for (int y = 0; y < 128; y++) {
                for (int z = 0; z < 16; z++) {
                    Assert.assertEquals(Material.AIR, chunk.getBlock(x, y, z).getMaterial());
                }
            }
        }
    }

    @Test (expectedExceptions = IllegalArgumentException.class)
    public void testInvalidBlock() {
        PocketChunk chunk = new PocketChunk(0, 0);
        chunk.setBlock(16, 128, 18, Material.DIRT);
    }
}
