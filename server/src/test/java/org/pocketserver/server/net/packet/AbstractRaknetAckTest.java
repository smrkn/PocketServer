package org.pocketserver.server.net.packet;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.buffer.Unpooled;

import java.util.Arrays;
import java.util.stream.IntStream;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class AbstractRaknetAckTest {
    @Test
    public void testSingularAck() throws Exception {
        test("Singular", 5);
    }

    @Test
    public void testSingularRangeAck() throws Exception {
        test("(Singular) Ranged", IntStream.rangeClosed(0, 10).toArray());
    }

    @Test
    public void testMultipleRangeAck() throws Exception {
        test("(Multiple) Ranged", IntStream.rangeClosed(0, 30).filter(in -> !(in > 10 && in < 20)).toArray());
    }

    private void test(String name, int... input) throws Exception {
        ByteBuf buf = Unpooled.buffer();

        AbstractRaknetAck packet = new PacketRaknetOutAck(input);
        packet.write(buf);

        System.out.printf("%s ACK: %s%n", name, ByteBufUtil.hexDump(buf).toUpperCase());
        packet.packets = null;

        packet.read(buf);
        System.out.printf("Acknowledged: %s%n", Arrays.toString(packet.packets));
        assertEquals(packet.packets, input, "decoded message numbers did not match");
    }
}