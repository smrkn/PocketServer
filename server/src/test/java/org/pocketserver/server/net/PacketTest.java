package org.pocketserver.server.net;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.net.InetAddress;
import java.net.InetSocketAddress;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class PacketTest {
    @Test
    public void testAddresses() throws Exception {
        ByteBuf buf = Unpooled.buffer();

        InetSocketAddress input = new InetSocketAddress(InetAddress.getLocalHost(), 19132);
        Packet.writeAddress(buf, input);
        InetSocketAddress output = Packet.readAddress(buf);

        assertEquals(input.getPort(), output.getPort(), "port does not match");
        assertEquals(input.getAddress(), output.getAddress(), "address does not match");
    }

    @Test
    public void testInt24() throws Exception {
        ByteBuf buf = Unpooled.buffer(3 * 0x00FFFFFF);

        for (int i = 0; i < 0x00FFFFFF; i++) {
            Packet.writeInt24(buf, i);
            assertEquals(Packet.readInt24(buf), i, "value does not match");
        }
    }
}