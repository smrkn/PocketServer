package org.pocketserver.server.event;

import org.pocketserver.event.Event;
import org.pocketserver.event.EventHandler;
import org.pocketserver.event.EventPipeline;
import org.pocketserver.event.EventSource;
import org.pocketserver.event.Priority;
import org.pocketserver.event.SimpleEventHandler;

import java.util.concurrent.atomic.AtomicInteger;

import org.testng.Assert;
import org.testng.annotations.Test;

public class EventTest {

    @Test
    public void testSimpleEventHandler() {
        EventHandler handler = new SampleSimpleEventHandler(new TypeMatcherSupplier());
        SampleEvent event = new OtherEvent();
        handler.eventTriggered(event, Priority.DEFAULT, null);
        Assert.assertTrue(event.isTriggered());

        event = new SampleEvent();
        handler.eventTriggered(event, Priority.DEFAULT, null);
        Assert.assertFalse(event.isTriggered());
    }

    @Test
    public void testPriority() {
        EventPipeline pipeline = new PocketEventPipeline();
        EventPipeline.PipelineEditor editor = pipeline.edit();
        AtomicInteger ctr = new AtomicInteger(0);
        editor.add((event, priority, source) -> Assert.assertEquals(0, ctr.getAndIncrement()), Priority.FIRST);
        editor.add((event, priority, source) -> Assert.assertEquals(1, ctr.getAndIncrement()));
        editor.add((event, priority, source) -> Assert.assertEquals(2, ctr.getAndIncrement()), Priority.LAST);
        editor.add((event, priority, source) -> Assert.assertEquals(3, ctr.get()), Priority.MONITOR);
        editor.finish();
        pipeline.fireEvent(new SampleEvent(), null);
    }

    private class SampleSimpleEventHandler extends SimpleEventHandler<OtherEvent> {

        public SampleSimpleEventHandler(TypeMatcherSupplier supplier) {
            super(supplier);
        }

        @Override
        protected void eventCalled(OtherEvent event) {
            event.setTriggered(true);
        }
    }

    private class OtherEvent extends SampleEvent {

    }

    private class SampleEvent extends Event {
        private boolean triggered = false;

        public boolean isTriggered() {
            return triggered;
        }

        public void setTriggered(boolean triggered) {
            this.triggered = triggered;
        }
    }
}
