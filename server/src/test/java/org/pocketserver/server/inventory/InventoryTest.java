package org.pocketserver.server.inventory;

import org.pocketserver.server.block.Material;
import org.pocketserver.server.item.ItemStack;
import org.pocketserver.server.item.inventory.PlayerInventory;
import org.pocketserver.server.item.inventory.types.CreativeInventory;
import org.pocketserver.server.item.inventory.types.SurvivalInventory;
import org.pocketserver.server.player.PocketPlayer;

import org.testng.Assert;
import org.testng.annotations.Test;

public class InventoryTest {
    private PocketPlayer player = new PocketPlayer("Nathan", null, 0, null, null);

    @Test(expectedExceptions = UnsupportedOperationException.class)
    public void testCreativeInventory() {
        PlayerInventory inventory = new CreativeInventory(player);
        if (inventory.getCapacity() != 112) {
            Assert.fail();
        }
        inventory.addItem(new ItemStack(Material.STONE, 1, 0));
    }

    @Test
    public void testSurvivalInventory() {
        PlayerInventory inventory = new SurvivalInventory(player);
        if (inventory.getCapacity() != 36) {
            Assert.fail();
        }
        for (int i = 0; i < inventory.getCapacity(); i++) {
            inventory.addItem(new ItemStack(Material.STONE, 1, 0));
        }
        Assert.assertEquals(inventory.getEmptyIndex(), -1);
    }

    @Test
    public void testInventoryAddingEmpty() {
        PlayerInventory inventory = new SurvivalInventory(player);
        inventory.addItem(new ItemStack(Material.AIR, 0, 0));
        Assert.assertEquals(inventory.getEmptyIndex(), 0);
        Assert.assertEquals(inventory.getSize(), 0);
        Assert.assertTrue(inventory.getContents().isEmpty());
    }
}
