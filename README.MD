PocketServer
============

We decided to start fresh because frankly things were a mess.
The old repository is still available online as `PocketServer/PocketServer-ref`.

## Contributing

Please review our [contributor guidelines](https://github.com/PocketServer/PocketServer/blob/master/CONTRIBUTING.MD) before creating a Pull Request.

## Acknowledgements

[![YourKit Logo](https://www.yourkit.com/images/yklogo.png)](https://yourkit.com)

We use YourKit's fantastic java profiler to ensure the server performs to the best of it's ability, you should too!
Check it out at [https://yourkit.com](https://yourkit.com)!
